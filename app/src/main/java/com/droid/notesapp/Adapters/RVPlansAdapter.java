package com.droid.notesapp.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.droid.notesapp.Models.CategoryModel;
import com.droid.notesapp.R;

import java.util.ArrayList;

/**
 * Created by Development on 4/18/2017.
 */

public class RVPlansAdapter extends RecyclerView.Adapter<RVPlansAdapter.ViewHolder> {

    private ArrayList<CategoryModel> arrListTopStatementss = new ArrayList<>();
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;

    // data is passed into the constructor
    public RVPlansAdapter(Context context, ArrayList<CategoryModel> arrListTopStatementss) {
        this.mInflater = LayoutInflater.from(context);
        this.arrListTopStatementss = arrListTopStatementss;
        this.context = context;
    }

    // inflates the cell layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_plan, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    // binds the data to the textview in each cell
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.txtTitle.setText(this.arrListTopStatementss.get(position).getTitle());
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return this.arrListTopStatementss.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
//        public CustomFontTextView tvAppointmentTitle, tvAppointmentDateTime, tvTopStatementsstatus, tvAppointmentNotes;
        public TextView txtTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClickC(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public CategoryModel getItem(int id) {
        return this.arrListTopStatementss.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClickC(View view, int position);
    }
}
