package com.droid.notesapp.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.droid.notesapp.Models.LectureModel;
import com.droid.notesapp.R;
import com.droid.notesapp.helper.FrontEngine;

import java.util.ArrayList;

/**
 * Created by Development on 4/18/2017.
 */

public class RVSearchLecturesAdapter extends RecyclerView.Adapter<RVSearchLecturesAdapter.ViewHolder> {

    private ArrayList<LectureModel> arrListLectures = new ArrayList<>();
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;

    // data is passed into the constructor
    public RVSearchLecturesAdapter(Context context, ArrayList<LectureModel> arrListLectures) {
        this.mInflater = LayoutInflater.from(context);
        this.arrListLectures = arrListLectures;
        this.context = context;
    }

    // inflates the cell layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_lecture_sc, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    // binds the data to the textview in each cell
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.txtTitle.setText(arrListLectures.get(position).getTitle());
        String timeStart, timeEnd;
        timeStart = FrontEngine.getInstance().secsToTimeFormat(arrListLectures.get(position).getTimestart());
        timeEnd = FrontEngine.getInstance().secsToTimeFormat(arrListLectures.get(position).getTimeend());
        holder.txtDuration.setText(timeStart +" - "+timeEnd);
        holder.txtCategory.setText(arrListLectures.get(position).getCategory());
        holder.txtDate.setText(FrontEngine.getInstance().secsToDayFormat(arrListLectures.get(position).getTimestart()));
        holder.txtMonth.setText(FrontEngine.getInstance().secsToMonthFormat(arrListLectures.get(position).getTimestart()));
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return this.arrListLectures.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        //        public CustomFontTextView tvAppointmentTitle, tvAppointmentDateTime, tvTopStatementsstatus, tvAppointmentNotes;
        public TextView txtTitle, txtDuration, txtCategory;
        public TextView txtDate, txtMonth;

        public ViewHolder(View itemView) {
            super(itemView);
            txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
            txtDuration = (TextView) itemView.findViewById(R.id.txtDuration);
            txtCategory = (TextView) itemView.findViewById(R.id.txtCategory);
            txtDate = (TextView) itemView.findViewById(R.id.txtDate);
            txtMonth = (TextView) itemView.findViewById(R.id.txtMonth);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClickC(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public LectureModel getItem(int id) {
        return this.arrListLectures.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClickC(View view, int position);
    }
}
