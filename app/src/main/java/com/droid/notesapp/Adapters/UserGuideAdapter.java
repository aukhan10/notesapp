package com.droid.notesapp.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.droid.notesapp.R;

import java.util.ArrayList;

/**
 * Created by Development on 11/18/2017.
 */

public class UserGuideAdapter extends PagerAdapter {

    private ArrayList<String> arrListUserGuide;
    private LayoutInflater inflater;
    private Context context;
    private ItemClickListener mClickListener;

    private ImageView ivUserGuide;
    private ProgressBar pbUserGuide;
    private Dialog dialog;

    public UserGuideAdapter(Context context, ArrayList<String> arrListUserGuide) {
        this.context = context;
        this.arrListUserGuide = arrListUserGuide;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return arrListUserGuide.size();
    }

    public String getItem(int id) {
        return this.arrListUserGuide.get(id);
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View myImageLayout = inflater.inflate(R.layout.item_date_pagers, view, false);

        TextView txtDate = myImageLayout.findViewById(R.id.txtDate);
        TextView txtMonth = myImageLayout.findViewById(R.id.txtMonth);

        String str = ""+arrListUserGuide.get(position).trim();
        String[] splited = str.split("\\s+");

        txtDate.setText(splited[0]);
        txtMonth.setText(splited[1]);


        view.addView(myImageLayout, 0);
        return myImageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    private void initializeControlsAndSetListener(int position) {
//        ivUserGuide = dialog.findViewById(R.id.ivUserGuide);
//        pbUserGuide = dialog.findViewById(R.id.pbUserGuide);
//        ((BaseActivity) context).objGlobalHelperNormal.setGlideNormalImage(context, ivUserGuide
//                , this.arrListUserGuide.get(position).getImgUserGuidemUrl(),
//                pbUserGuide, R.mipmap.ic_launcher);
//        ivUserGuide.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
