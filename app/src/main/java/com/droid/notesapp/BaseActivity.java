package com.droid.notesapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.droid.notesapp.helper.GlobalHelperNormal;
import com.droid.notesapp.helper.SharedPreference;
import com.droid.notesapp.helper.Validation;


public class BaseActivity extends AppCompatActivity {

    public GlobalHelperNormal objGlobalHelperNormal;
    protected SharedPreference prefs;
    public Validation objValidation;
    public DBHelper mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        objGlobalHelperNormal = GlobalHelperNormal.getInstance();
        prefs = SharedPreference.getInstance(this);
        objValidation = Validation.getInstance();
        mDatabase = new DBHelper(this);
    }
}
