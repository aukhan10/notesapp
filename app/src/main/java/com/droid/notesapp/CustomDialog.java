package com.droid.notesapp;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class CustomDialog extends Dialog {

    private Activity activity;
    private Button ok;
    private String title, desciption;
    private OnClickSelection inter;
    private TextView heading, desc;

    private Handler mHandler = null;
    private int type;


    public CustomDialog(Activity activity, String title, String desc, OnClickSelection inter) {
        super(activity, android.R.style.Theme_Translucent_NoTitleBar);
        this.activity = activity;
        this.title = title;
        this.desciption = desc;
        this.inter = inter;
    }


    public CustomDialog(BaseActivity activity, int style) {
        super(activity, style);
    }

    @Override
    public void show() {
        super.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fullScreenCode();
        setContentView(R.layout.dialog_custom);

        initViews();

    }

    protected void fullScreenCode() {

        Window window = this.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

    }

    private void initViews() {

        heading = (TextView)findViewById(R.id.heading);
        desc = (TextView)findViewById(R.id.desc);
        ok = (Button)findViewById(R.id.ok);

        if(!TextUtils.isEmpty(title)){
            heading.setText(""+title);
        }

        if(!TextUtils.isEmpty(desciption)){
            desc.setText(""+desciption);
        }

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inter.setOk();
            }
        });


    }

    public interface OnClickSelection {
        public void setOk();
    }




}
