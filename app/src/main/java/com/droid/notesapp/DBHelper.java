package com.droid.notesapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import com.droid.notesapp.Models.LectureModel;
import com.droid.notesapp.helper.FrontEngine;
import com.droid.notesapp.listeners.OnDatabaseChangedListener;

import java.util.ArrayList;
import java.util.Comparator;

public class DBHelper extends SQLiteOpenHelper {
    private Context mContext;

    private static final String LOG_TAG = "DBHelper";

    private static OnDatabaseChangedListener mOnDatabaseChangedListener;

    public static final String DATABASE_NAME = "saved_recordings.db";
    private static final int DATABASE_VERSION = 1;

    public static abstract class DBHelperItem implements BaseColumns {
        public static final String TABLE_NAME = "saved_recordings";

        public static final String COLUMN_NAME_LECTURE_TITLE = "lecture_title";
        public static final String COLUMN_NAME_LECTURE_CATEGORY = "lecture_category";
        public static final String COLUMN_NAME_LECTURE_DATE = "lecture_date";
        public static final String COLUMN_NAME_LECTURE_DESC = "lecture_desc";
        public static final String COLUMN_NAME_LECTURE_BOOKMARK = "lecture_bookmark";
        public static final String COLUMN_NAME_RECORDING_NAME = "recording_name";
        public static final String COLUMN_NAME_RECORDING_FILE_PATH = "file_path";
        public static final String COLUMN_NAME_RECORDING_LENGTH = "length";
        public static final String COLUMN_NAME_TIME_START = "time_start";
        public static final String COLUMN_NAME_TIME_END = "time_end";
    }

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + DBHelperItem.TABLE_NAME + " (" +
                    DBHelperItem._ID + " INTEGER PRIMARY KEY" + COMMA_SEP +
                    DBHelperItem.COLUMN_NAME_LECTURE_TITLE + TEXT_TYPE + COMMA_SEP +
                    DBHelperItem.COLUMN_NAME_LECTURE_CATEGORY + TEXT_TYPE + COMMA_SEP +
                    DBHelperItem.COLUMN_NAME_LECTURE_DATE + TEXT_TYPE + COMMA_SEP +
                    DBHelperItem.COLUMN_NAME_LECTURE_DESC + TEXT_TYPE + COMMA_SEP +
                    DBHelperItem.COLUMN_NAME_LECTURE_BOOKMARK + " INTEGER " + COMMA_SEP +
                    DBHelperItem.COLUMN_NAME_RECORDING_NAME + TEXT_TYPE + COMMA_SEP +
                    DBHelperItem.COLUMN_NAME_RECORDING_FILE_PATH + TEXT_TYPE + COMMA_SEP +
                    DBHelperItem.COLUMN_NAME_RECORDING_LENGTH + " INTEGER " + COMMA_SEP +
                    DBHelperItem.COLUMN_NAME_TIME_START + " INTEGER " + COMMA_SEP +
                    DBHelperItem.COLUMN_NAME_TIME_END + " INTEGER " + ")";

    @SuppressWarnings("unused")
    private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + DBHelperItem.TABLE_NAME;

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
    }

    public static void setOnDatabaseChangedListener(OnDatabaseChangedListener listener) {
        mOnDatabaseChangedListener = listener;
    }

    private ArrayList<LectureModel> arrListLectures;

    String[] projection = {
            DBHelperItem._ID,
            DBHelperItem.COLUMN_NAME_LECTURE_TITLE,
            DBHelperItem.COLUMN_NAME_LECTURE_CATEGORY,
            DBHelperItem.COLUMN_NAME_LECTURE_DATE,
            DBHelperItem.COLUMN_NAME_LECTURE_DESC,
            DBHelperItem.COLUMN_NAME_LECTURE_BOOKMARK,
            DBHelperItem.COLUMN_NAME_RECORDING_NAME,
            DBHelperItem.COLUMN_NAME_RECORDING_FILE_PATH,
            DBHelperItem.COLUMN_NAME_RECORDING_LENGTH,
            DBHelperItem.COLUMN_NAME_TIME_START,
            DBHelperItem.COLUMN_NAME_TIME_END
    };

    public LectureModel getItemAt(int position, String selection) {
        SQLiteDatabase db = getReadableDatabase();

        Cursor c = db.query(DBHelperItem.TABLE_NAME, projection, selection, null, null, null, null);
        if (c.moveToPosition(position)) {
            LectureModel item = new LectureModel();
            item.setId(c.getInt(c.getColumnIndex(DBHelperItem._ID)));
            item.setTitle(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_LECTURE_TITLE)));
            item.setCategory(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_LECTURE_CATEGORY)));
            item.setDate(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_LECTURE_DATE)));
            item.setDecsricption(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_LECTURE_DESC)));
            item.setBookmark(c.getInt(c.getColumnIndex(DBHelperItem.COLUMN_NAME_LECTURE_BOOKMARK)));
            item.setName(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_RECORDING_NAME)));
            item.setFilePath(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_RECORDING_FILE_PATH)));
            item.setLength(c.getInt(c.getColumnIndex(DBHelperItem.COLUMN_NAME_RECORDING_LENGTH)));
            item.setTimestart(c.getLong(c.getColumnIndex(DBHelperItem.COLUMN_NAME_TIME_START)));
            item.setTimeend(c.getLong(c.getColumnIndex(DBHelperItem.COLUMN_NAME_TIME_END)));
            c.close();
            return item;
        }
        return null;
    }

    public ArrayList<LectureModel> getAllLectures() {
        arrListLectures = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        Cursor c = db.query(DBHelperItem.TABLE_NAME, projection, null, null, null, null, null);
        for (int i = 0; i < c.getCount(); i++) {
            if (c.moveToPosition(i)) {
                LectureModel item = new LectureModel();
                item.setId(c.getInt(c.getColumnIndex(DBHelperItem._ID)));
                item.setTitle(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_LECTURE_TITLE)));
                item.setCategory(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_LECTURE_CATEGORY)));
                item.setDate(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_LECTURE_DATE)));
                item.setDecsricption(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_LECTURE_DESC)));
                item.setBookmark(c.getInt(c.getColumnIndex(DBHelperItem.COLUMN_NAME_LECTURE_BOOKMARK)));
                item.setName(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_RECORDING_NAME)));
                item.setFilePath(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_RECORDING_FILE_PATH)));
                item.setLength(c.getInt(c.getColumnIndex(DBHelperItem.COLUMN_NAME_RECORDING_LENGTH)));
                item.setTimestart(c.getLong(c.getColumnIndex(DBHelperItem.COLUMN_NAME_TIME_START)));
                item.setTimeend(c.getLong(c.getColumnIndex(DBHelperItem.COLUMN_NAME_TIME_END)));

                arrListLectures.add(item);
            }
        }
        c.close();
        return arrListLectures;

    }

    public ArrayList<LectureModel> getDateLectures(String date) {
        String selection = DBHelperItem.COLUMN_NAME_LECTURE_DATE + "= '" + date + "'";
        arrListLectures = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        Cursor c = db.query(DBHelperItem.TABLE_NAME, projection, selection, null, null, null, null);
        for (int i = 0; i < c.getCount(); i++) {
            if (c.moveToPosition(i)) {
                LectureModel item = new LectureModel();
                item.setId(c.getInt(c.getColumnIndex(DBHelperItem._ID)));
                item.setTitle(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_LECTURE_TITLE)));
                item.setCategory(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_LECTURE_CATEGORY)));
                item.setDate(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_LECTURE_DATE)));
                item.setDecsricption(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_LECTURE_DESC)));
                item.setBookmark(c.getInt(c.getColumnIndex(DBHelperItem.COLUMN_NAME_LECTURE_BOOKMARK)));
                item.setName(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_RECORDING_NAME)));
                item.setFilePath(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_RECORDING_FILE_PATH)));
                item.setLength(c.getInt(c.getColumnIndex(DBHelperItem.COLUMN_NAME_RECORDING_LENGTH)));
                item.setTimestart(c.getLong(c.getColumnIndex(DBHelperItem.COLUMN_NAME_TIME_START)));
                item.setTimeend(c.getLong(c.getColumnIndex(DBHelperItem.COLUMN_NAME_TIME_END)));

                arrListLectures.add(item);
            }
        }
        c.close();
        return arrListLectures;

    }

    public ArrayList<LectureModel> getCategoryLectures(String category) {
        String selection = DBHelperItem.COLUMN_NAME_LECTURE_CATEGORY + "= '" + category + "'";
        arrListLectures = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        Cursor c = db.query(DBHelperItem.TABLE_NAME, projection, selection, null, null, null, null);
        for (int i = 0; i < c.getCount(); i++) {
            if (c.moveToPosition(i)) {
                LectureModel item = new LectureModel();
                item.setId(c.getInt(c.getColumnIndex(DBHelperItem._ID)));
                item.setTitle(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_LECTURE_TITLE)));
                item.setCategory(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_LECTURE_CATEGORY)));
                item.setDate(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_LECTURE_DATE)));
                item.setDecsricption(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_LECTURE_DESC)));
                item.setBookmark(c.getInt(c.getColumnIndex(DBHelperItem.COLUMN_NAME_LECTURE_BOOKMARK)));
                item.setName(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_RECORDING_NAME)));
                item.setFilePath(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_RECORDING_FILE_PATH)));
                item.setLength(c.getInt(c.getColumnIndex(DBHelperItem.COLUMN_NAME_RECORDING_LENGTH)));
                item.setTimestart(c.getLong(c.getColumnIndex(DBHelperItem.COLUMN_NAME_TIME_START)));
                item.setTimeend(c.getLong(c.getColumnIndex(DBHelperItem.COLUMN_NAME_TIME_END)));

                arrListLectures.add(item);
            }
        }
        c.close();
        return arrListLectures;

    }

    public ArrayList<LectureModel> getBookmarkLectures() {
        String selection = DBHelperItem.COLUMN_NAME_LECTURE_BOOKMARK + "= '1'";
        arrListLectures = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        Cursor c = db.query(DBHelperItem.TABLE_NAME, projection, selection, null, null, null, null);
        for (int i = 0; i < c.getCount(); i++) {
            if (c.moveToPosition(i)) {
                LectureModel item = new LectureModel();
                item.setId(c.getInt(c.getColumnIndex(DBHelperItem._ID)));
                item.setTitle(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_LECTURE_TITLE)));
                item.setCategory(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_LECTURE_CATEGORY)));
                item.setDate(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_LECTURE_DATE)));
                item.setDecsricption(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_LECTURE_DESC)));
                item.setBookmark(c.getInt(c.getColumnIndex(DBHelperItem.COLUMN_NAME_LECTURE_BOOKMARK)));
                item.setName(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_RECORDING_NAME)));
                item.setFilePath(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_RECORDING_FILE_PATH)));
                item.setLength(c.getInt(c.getColumnIndex(DBHelperItem.COLUMN_NAME_RECORDING_LENGTH)));
                item.setTimestart(c.getLong(c.getColumnIndex(DBHelperItem.COLUMN_NAME_TIME_START)));
                item.setTimeend(c.getLong(c.getColumnIndex(DBHelperItem.COLUMN_NAME_TIME_END)));

                arrListLectures.add(item);
            }
        }
        c.close();
        return arrListLectures;

    }

    public ArrayList<LectureModel> getSearchLectures(String filter) {
        String selection = DBHelperItem.COLUMN_NAME_LECTURE_TITLE + " LIKE ?";
        arrListLectures = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        Cursor c = db.query(DBHelperItem.TABLE_NAME, projection, selection, new String[]{"%" + filter + "%"}, null, null, null, null);
        for (int i = 0; i < c.getCount(); i++) {
            if (c.moveToPosition(i)) {
                LectureModel item = new LectureModel();
                item.setId(c.getInt(c.getColumnIndex(DBHelperItem._ID)));
                item.setTitle(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_LECTURE_TITLE)));
                item.setCategory(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_LECTURE_CATEGORY)));
                item.setDate(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_LECTURE_DATE)));
                item.setDecsricption(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_LECTURE_DESC)));
                item.setBookmark(c.getInt(c.getColumnIndex(DBHelperItem.COLUMN_NAME_LECTURE_BOOKMARK)));
                item.setName(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_RECORDING_NAME)));
                item.setFilePath(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_RECORDING_FILE_PATH)));
                item.setLength(c.getInt(c.getColumnIndex(DBHelperItem.COLUMN_NAME_RECORDING_LENGTH)));
                item.setTimestart(c.getLong(c.getColumnIndex(DBHelperItem.COLUMN_NAME_TIME_START)));
                item.setTimeend(c.getLong(c.getColumnIndex(DBHelperItem.COLUMN_NAME_TIME_END)));

                arrListLectures.add(item);
            }
        }
        c.close();
        return arrListLectures;

    }

    public ArrayList<String> getDates() {
        ArrayList<String> arrListDates = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        Cursor c = db.query(DBHelperItem.TABLE_NAME, new String[]{DBHelperItem.COLUMN_NAME_LECTURE_DATE}, null, null, DBHelperItem.COLUMN_NAME_LECTURE_DATE, null, null);
        for (int i = 0; i < c.getCount(); i++) {
            if (c.moveToPosition(i)) {
//                LectureModel item = new LectureModel();
//                item.setId(c.getInt(c.getColumnIndex(DBHelperItem._ID)));
//                item.setTitle(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_LECTURE_TITLE)));
//                item.setCategory(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_LECTURE_CATEGORY)));
//                item.setDate(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_LECTURE_DATE)));
//                item.setName(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_RECORDING_NAME)));
//                item.setFilePath(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_RECORDING_FILE_PATH)));
//                item.setLength(c.getInt(c.getColumnIndex(DBHelperItem.COLUMN_NAME_RECORDING_LENGTH)));
//                item.setTimestart(c.getLong(c.getColumnIndex(DBHelperItem.COLUMN_NAME_TIME_START)));
//                item.setTimeend(c.getLong(c.getColumnIndex(DBHelperItem.COLUMN_NAME_TIME_END)));

                arrListDates.add(c.getString(c.getColumnIndex(DBHelperItem.COLUMN_NAME_LECTURE_DATE)));
            }
        }
        c.close();
        return arrListDates;

    }

    public ArrayList<LectureModel> getRecords() {
        arrListLectures = new ArrayList<>();

        for (int i = 0; i < getCount(); i++) {
            arrListLectures.add(getItemAt(i, null));
        }

        return arrListLectures;
    }

    public ArrayList<LectureModel> getRecordsCategory(String category) {
        arrListLectures = new ArrayList<>();
        String selection = DBHelperItem.COLUMN_NAME_LECTURE_CATEGORY + "= '" + category + "'";
        for (int i = 0; i < getCount(selection); i++) {
            arrListLectures.add(getItemAt(i, selection));
        }

        return arrListLectures;
    }

    public void removeItemWithId(int id) {
        SQLiteDatabase db = getWritableDatabase();
        String[] whereArgs = {String.valueOf(id)};
        db.delete(DBHelperItem.TABLE_NAME, "_ID=?", whereArgs);
    }

    private int getCount(String selection) {
        SQLiteDatabase db = getReadableDatabase();
        String[] projection = {DBHelperItem._ID};
        Cursor c = db.query(DBHelperItem.TABLE_NAME, projection, selection, null, null, null, null);
        int count = c.getCount();
        c.close();
        return count;
    }

    public int getCount() {
        SQLiteDatabase db = getReadableDatabase();
        String[] projection = {DBHelperItem._ID};
        Cursor c = db.query(DBHelperItem.TABLE_NAME, projection, null, null, null, null, null);
        int count = c.getCount();
        c.close();
        return count;
    }

    public Context getContext() {
        return mContext;
    }

//    public class RecordingComparator implements Comparator<LectureModel> {
//        public int compare(LectureModel item1, LectureModel item2) {
//            Long o1 = item1.getTime();
//            Long o2 = item2.getTime();
//            return o2.compareTo(o1);
//        }
//    }

    public long addRecording(String title, String category, String recordingName, String filePath, long length, long timeStart) {

        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(DBHelperItem.COLUMN_NAME_LECTURE_TITLE, title);
        cv.put(DBHelperItem.COLUMN_NAME_LECTURE_CATEGORY, category);
        cv.put(DBHelperItem.COLUMN_NAME_LECTURE_DATE, FrontEngine.getInstance().secsToDateFormat(System.currentTimeMillis()));
        cv.put(DBHelperItem.COLUMN_NAME_LECTURE_DESC, "Lorem ipsum");
        cv.put(DBHelperItem.COLUMN_NAME_LECTURE_BOOKMARK, 0);
        cv.put(DBHelperItem.COLUMN_NAME_RECORDING_NAME, recordingName);
        cv.put(DBHelperItem.COLUMN_NAME_RECORDING_FILE_PATH, filePath);
        cv.put(DBHelperItem.COLUMN_NAME_RECORDING_LENGTH, length);
        cv.put(DBHelperItem.COLUMN_NAME_TIME_START, timeStart);
        cv.put(DBHelperItem.COLUMN_NAME_TIME_END, System.currentTimeMillis());
        long rowId = db.insert(DBHelperItem.TABLE_NAME, null, cv);

        if (mOnDatabaseChangedListener != null) {
            mOnDatabaseChangedListener.onNewDatabaseEntryAdded();
        }

        return rowId;
    }

//    public long addRecording(LectureModel lectureModel) {
//
//        SQLiteDatabase db = getWritableDatabase();
//        ContentValues cv = new ContentValues();
//        cv.put(DBHelperItem.COLUMN_NAME_LECTURE_TITLE, lectureModel.getTitle());
//        cv.put(DBHelperItem.COLUMN_NAME_LECTURE_CATEGORY, lectureModel.getCategory());
//        cv.put(DBHelperItem.COLUMN_NAME_RECORDING_NAME, lectureModel.getName());
//        cv.put(DBHelperItem.COLUMN_NAME_RECORDING_FILE_PATH, lectureModel.getFilePath());
//        cv.put(DBHelperItem.COLUMN_NAME_RECORDING_LENGTH, lectureModel.getLength());
//        cv.put(DBHelperItem.COLUMN_NAME_TIME_END, System.currentTimeMillis());
//        long rowId = db.insert(DBHelperItem.TABLE_NAME, null, cv);
//
//        if (mOnDatabaseChangedListener != null) {
//            mOnDatabaseChangedListener.onNewDatabaseEntryAdded();
//        }
//
//        return rowId;
//    }

    public void setBookmark(LectureModel item, int isBookmark) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(DBHelperItem.COLUMN_NAME_LECTURE_BOOKMARK, isBookmark);
        db.update(DBHelperItem.TABLE_NAME, cv,
                DBHelperItem._ID + "=" + item.getId(), null);

        if (mOnDatabaseChangedListener != null) {
            mOnDatabaseChangedListener.onDatabaseEntryRenamed();
        }
    }
//    public void renameItem(LectureModel item, String title, String category, String recordingName, String filePath) {
//        SQLiteDatabase db = getWritableDatabase();
//        ContentValues cv = new ContentValues();
//        cv.put(DBHelperItem.COLUMN_NAME_LECTURE_TITLE, title);
//        cv.put(DBHelperItem.COLUMN_NAME_LECTURE_CATEGORY, category);
//        cv.put(DBHelperItem.COLUMN_NAME_RECORDING_NAME, recordingName);
//        cv.put(DBHelperItem.COLUMN_NAME_RECORDING_FILE_PATH, filePath);
//        db.update(DBHelperItem.TABLE_NAME, cv,
//                DBHelperItem._ID + "=" + item.getId(), null);
//
//        if (mOnDatabaseChangedListener != null) {
//            mOnDatabaseChangedListener.onDatabaseEntryRenamed();
//        }
//    }

//    public long restoreRecording(LectureModel item) {
//        SQLiteDatabase db = getWritableDatabase();
//        ContentValues cv = new ContentValues();
//        cv.put(DBHelperItem.COLUMN_NAME_LECTURE_TITLE, item.getTitle());
//        cv.put(DBHelperItem.COLUMN_NAME_LECTURE_CATEGORY, item.getCategory());
//        cv.put(DBHelperItem.COLUMN_NAME_RECORDING_NAME, item.getName());
//        cv.put(DBHelperItem.COLUMN_NAME_RECORDING_FILE_PATH, item.getFilePath());
//        cv.put(DBHelperItem.COLUMN_NAME_RECORDING_LENGTH, item.getLength());
//        cv.put(DBHelperItem.COLUMN_NAME_TIME_END, item.getTime());
//        cv.put(DBHelperItem._ID, item.getId());
//        long rowId = db.insert(DBHelperItem.TABLE_NAME, null, cv);
//        if (mOnDatabaseChangedListener != null) {
//            //mOnDatabaseChangedListener.onNewDatabaseEntryAdded();
//        }
//        return rowId;
//    }
}
