package com.droid.notesapp;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.droid.notesapp.fragments.AboutFragment;
import com.droid.notesapp.fragments.BookmarksFragment;
import com.droid.notesapp.fragments.BrowseCategoriesFragment;
import com.droid.notesapp.fragments.FeedbackFragment;
import com.droid.notesapp.fragments.LecturesFragment;
import com.droid.notesapp.fragments.PlansFragment;
import com.droid.notesapp.fragments.PrivacyPolicyFragment;
import com.droid.notesapp.fragments.StartLectureFragment;
import com.droid.notesapp.helper.FrontEngine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends BaseActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    //Header
    public static LinearLayout headerLl, llMenu1,llMenu2;
    public static TextView tv_title, tvMenu3, tvMenu4;
    public static ImageView ivMenu1, ivMenu2;

    //Drawer
    public static LinearLayout ll_profile;
    TextView user_name;
    ImageView img_profile;
    ProgressBar pbar;
    private DrawerLayout drawer;
    private LinearLayout ll_edit_profile;

    private String fragmentName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeControls();
        setOnClickListeners();
        setMenuBarAndLoadItems();
        SelectItem(0);
        boolean result = new CheckPermissions().checkPermissionExternelStorageAndRecordAudio(this, this);
    }

    public void initializeControls() {
        tv_title = (TextView) findViewById(R.id.tv_title);

        headerLl = (LinearLayout) findViewById(R.id.headerLl);

        llMenu1 = (LinearLayout) findViewById(R.id.llMenu1);
        llMenu2 = (LinearLayout) findViewById(R.id.llMenu2);

        ivMenu1 = (ImageView) findViewById(R.id.ivMenu1);
        ivMenu2 = (ImageView) findViewById(R.id.ivMenu2);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

    }

    public void setOnClickListeners() {

        //Header Buttons Left
        llMenu1.setOnClickListener(this);
        llMenu2.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llMenu1:
                openAndCloseDrawer(true);
                break;
//            case R.id.tvYes:
//                dialogYes();
//                break;
//            case R.id.tvCancel:
//                dialogCancel();
//                break;
        }
    }

    public void openAndCloseDrawer(boolean menu) {
//        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            Animation startRotateAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.close_menu_rotate_animation);
//            llMenu1.startAnimation(startRotateAnimation);
            drawer.closeDrawer(GravityCompat.START);
        } else if (menu) {
//            Animation startRotateAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.open_menu_rotate_animation);
//            llMenu1.startAnimation(startRotateAnimation);
            drawer.openDrawer(GravityCompat.START);
        }
    }

    public void closeDrawer() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    public void setMenuBarAndLoadItems() {
        ArrayList<String> dataList = new ArrayList<String>();
        ListView mDrawerList = (ListView) findViewById(R.id.left_drawer);
        // mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
        // GravityCompat.START);


        dataList.add("Start Lecture"); // 0
        dataList.add("Lectures"); // 1
        dataList.add("Browse Categories"); // 2
        dataList.add("Bookmarks"); // 3
        dataList.add("Send Feedback"); // 4
        dataList.add("Share App"); // 5
        dataList.add("Rate App"); // 6
        dataList.add("Upgrade to Premium"); // 7
        dataList.add("Privacy Policy / T&C"); // 8
        dataList.add("About"); // 9


        ArrayAdapter adapter = new ArrayAdapter<String>(MainActivity.this, R.layout.menu_item1, dataList);

        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(this);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        SelectItem(position);
//        Toast.makeText(this, "List Item " + position, Toast.LENGTH_SHORT).show();
    }

    public void SelectItem(int position) {
        Fragment fragmentNav = null;
        switch (position) {
            case 0: // Start Lecture

                closeDrawer();
                fragmentNav = new StartLectureFragment();

                break;

            case 1: // Lectures

                closeDrawer();
                fragmentNav = new LecturesFragment();

                break;

            case 2: // Browse Categories

                closeDrawer();
                fragmentNav = new BrowseCategoriesFragment();

                break;

            case 3: // Bookmarks

                closeDrawer();
                fragmentNav = new BookmarksFragment();

                break;
//            case 4: // Settings
//
//                closeDrawer();
////                fragmentNav = new TextbooksFragment();
//
//                break;

            case 4: // Send Feedback

                closeDrawer();
                fragmentNav = new FeedbackFragment();
                break;

            case 5: // Share App

                closeDrawer();
//                fragmentNav = new PurshasedFragment();
                break;

            case 6: // Rate App

                closeDrawer();
//                fragmentNav = new HelpFragment();
                break;

            case 7: // Upgrade to Premium

                closeDrawer();
                fragmentNav = new PlansFragment();
                break;

            case 8: // Privicy Policy / T&C

                closeDrawer();
                fragmentNav = new PrivacyPolicyFragment();
                break;


            case 9: // About

                closeDrawer();
                fragmentNav = new AboutFragment();

                break;

        }

//        if (position != 6 || position != 4) {
        replaceFragment(fragmentNav, this);
//        }

    }

    public void replaceFragment(Fragment currentFragment, FragmentActivity fragmentActivity) {
        try {
            objGlobalHelperNormal.hideSoftKeyboard(fragmentActivity);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (currentFragment != null && (!fragmentName.equals(currentFragment.getClass().getName()))

                ) {
            llMenu1.setOnClickListener(null);
            llMenu2.setOnClickListener(null);
            try {
                String backStateName = currentFragment.getClass().getName();
                FragmentManager manager = getSupportFragmentManager();

                boolean fragmentPopped = false;
                if (!backStateName.equals("com.appocta.a2ulearn.fragments.LessonOneFragment")) {
                    fragmentPopped = manager.popBackStackImmediate(backStateName, 0);
                }

//                boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

                if (!fragmentPopped) { //fragment not in back stack, create it.
                    FragmentTransaction ft = manager.beginTransaction();
                    ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                    ft.replace(R.id.content_frame, currentFragment, backStateName);
                    ft.addToBackStack(backStateName);
                    ft.commit();
                }
//                footerUIChange(backStateName);
                fragmentName = backStateName;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
//        FrontEngine.getInstance().objLocationsModel = null;
//        FrontEngine.getInstance().objPropertyModel = null;
        try {
            objGlobalHelperNormal.hideSoftKeyboard(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
                finish();
            } else {
//                fragmentName = getSupportFragmentManager().getFragments().
//                        get(getSupportFragmentManager().getBackStackEntryCount()-1).getClass().getName();
                String myFragmentName = getSupportFragmentManager().getBackStackEntryAt(
                        getSupportFragmentManager().getBackStackEntryCount() - 2).getName();
                if (!myFragmentName.equals("com.appocta.a2ulearn.fragments.LessonOneFragment")
                        && !myFragmentName.equals("com.appocta.a2ulearn.fragments.LessonTwoFragment")
                        && !myFragmentName.equals("com.appocta.a2ulearn.fragments.LessonThreeFragment")
                        && !myFragmentName.equals("com.appocta.a2ulearn.fragments.LessonFourFragment")
                        && !myFragmentName.equals("com.appocta.a2ulearn.fragments.LessonResultFragment")) {
                    fragmentName = myFragmentName;
                    super.onBackPressed();
                } else {
//                    if (FrontEngine.getInstance().getLessonType().equals("home")) {
//                        replaceFragment(new LessonListFragment(), this);
//                    } else if (FrontEngine.getInstance().getLessonType().equals("goal")) {
//                        replaceFragment(new GoalsFragment(), this);
//                    }
                }
            }
        }

    }

    public void initialWorkForAllFragments(Context c, String title,
                                           int one, int oneImg, int two, int twoImg) {
        tv_title.setText(title);
        ivMenu1.setVisibility(one);
        ivMenu2.setVisibility(two);
        ivMenu1.setImageResource(oneImg);
        ivMenu2.setImageResource(twoImg);
    }

}
