package com.droid.notesapp.Models;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LectureModel implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("decsricption")
    @Expose
    private String decsricption;
    @SerializedName("bookmark")
    @Expose
    private Integer bookmark;

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("filepath")
    @Expose
    private String filepath;
    @SerializedName("length")
    @Expose
    private Integer length;
    @SerializedName("timestart")
    @Expose
    private Long timestart;
    @SerializedName("timeend")
    @Expose
    private Long timeend;

    /**
     * No args constructor for use in serialization
     */
    public LectureModel() {
    }

    /**
     * @param id
     * @param filepath
     * @param time
     * @param title
     * @param category
     * @param name
     * @param length
     */
//    public LectureModel(Integer id, String title, String category, String name, String filepath, Integer length, Long timestart,Long timestart) {
//        super();
//        this.id = id;
//        this.title = title;
//        this.category = category;
//        this.name = name;
//        this.filepath = filepath;
//        this.length = length;
//        this.timestart = timestart;
//    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDecsricption() {
        return decsricption;
    }

    public void setDecsricption(String decsricption) {
        this.decsricption = decsricption;
    }

    public Integer getBookmark() {
        return bookmark;
    }

    public void setBookmark(Integer bookmark) {
        this.bookmark = bookmark;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFilePath() {
        return filepath;
    }

    public void setFilePath(String filepath) {
        this.filepath = filepath;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Long getTimestart() {
        return timestart;
    }

    public void setTimestart(Long timestart) {
        this.timestart = timestart;
    }

    public Long getTimeend() {
        return timeend;
    }

    public void setTimeend(Long timeend) {
        this.timeend = timeend;
    }
}

