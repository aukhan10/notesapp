package com.droid.notesapp.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserData implements Serializable {

    //Chating Start
    @SerializedName("isLoggedIn")
    @Expose
    private Integer isLoggedIn = 0;

    private String firstName =  "";
    private String lastName =  "";

    @SerializedName("user_name")
    @Expose
    private String user_name =  "";

    @SerializedName("udid")
    @Expose
    private String udid;
    @SerializedName("device_token")
    @Expose
    private String deviceToken;
    @SerializedName("device_type")
    @Expose
    private String deviceType =  "-";

    @SerializedName("totalnotification")
    @Expose
    private int totalNotification = 0;

    public int getTotalNotification() {
        return totalNotification;
    }

    public void setTotalNotification(int totalNotification) {
        this.totalNotification = totalNotification;
    }

    public Integer getIsLoggedIn() {
        return isLoggedIn;
    }

    public void setIsLoggedIn(Integer isLoggedIn) {
        this.isLoggedIn = isLoggedIn;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUdid() {
        return udid;
    }

    public void setUdid(String udid) {
        this.udid = udid;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }
    //Chating End

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("is_active")
    @Expose
    private String is_active;
    @SerializedName("access_token")
    @Expose
    private String access_token;
    @SerializedName("image_url")
    @Expose
    private String image_url;
    @SerializedName("social_id")
    @Expose
    private String social_id;
    @SerializedName("totalscore")
    @Expose
    private String totalscore;
    private final static long serialVersionUID = 2188509000967436392L;

    /**
     * No args constructor for use in serialization
     */
    public UserData() {
    }

    /**
     * @param id
     * @param image_url
     * @param is_active
     * @param email
     * @param name
     * @param social_id
     * @param access_token
     */
    public UserData(String id, String name, String email, String is_active, String access_token, String image_url, String social_id) {
        super();
        this.id = id;
        this.name = name;
        this.email = email;
        this.is_active = is_active;
        this.access_token = access_token;
        this.image_url = image_url;
        this.social_id = social_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getSocial_id() {
        return social_id;
    }

    public void setSocial_id(String social_id) {
        this.social_id = social_id;
    }

    public String getTotalscore() {
        return totalscore;
    }

    public void setTotalscore(String totalscore) {
        this.totalscore = totalscore;
    }
}