package com.droid.notesapp.Models;

/**
 * Created by Development on 11/18/2017.
 */

public class UserGuideModel {

    private String imgUserGuidemUrl;

    public UserGuideModel(String imgUserGuidemUrl) {
        this.imgUserGuidemUrl = imgUserGuidemUrl;
    }

    public String getImgUserGuidemUrl() {
        return imgUserGuidemUrl;
    }

    public void setImgUserGuidemUrl(String imgUserGuidemUrl) {
        this.imgUserGuidemUrl = imgUserGuidemUrl;
    }
}
