package com.droid.notesapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.droid.notesapp.Adapters.UserGuideAdapter;
import com.droid.notesapp.animations.CustPagerTransformer;
import com.droid.notesapp.helper.FixedSpeedScroller;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class TestActivity extends BaseActivity {

    private ArrayList<String> arrListUserGuide = new ArrayList<>();
    private ViewPager viewPagerUserGuide;
    UserGuideAdapter userGuideAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        setData();
        setUpUserGuideViewPagerWithAdapter();
        setPageScrollSpeedAndAnimation();
    }

    public void setData() {

        arrListUserGuide = new ArrayList<>();
        arrListUserGuide.add("21");
        arrListUserGuide.add("22");
        arrListUserGuide.add("23");
        arrListUserGuide.add("24");
        arrListUserGuide.add("25");
        arrListUserGuide.add("21");
        arrListUserGuide.add("22");
        arrListUserGuide.add("23");
        arrListUserGuide.add("24");
        arrListUserGuide.add("25");

    }

    private void setUpUserGuideViewPagerWithAdapter() {
        viewPagerUserGuide = findViewById(R.id.viewPagerLessons);
        userGuideAdapter = new UserGuideAdapter(this, arrListUserGuide);
        viewPagerUserGuide.setAdapter(userGuideAdapter);
//        userGuideAdapter.setClickListener(this);
//        CircleIndicator indicatorUserGuide = findViewById(R.id.indicatorUserGuide);
//        indicatorUserGuide.setViewPager(viewPagerUserGuide);


    }

    private void setPageScrollSpeedAndAnimation() {
        Field mScroller = null;
        // this work of scroller is done to slow the speed of scroll...
        try {
            mScroller = ViewPager.class.getDeclaredField("mScroller");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        mScroller.setAccessible(true);
        FixedSpeedScroller scroller = new FixedSpeedScroller(viewPagerUserGuide.getContext(), 1000);

        try {
            mScroller.set(viewPagerUserGuide, scroller);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        viewPagerUserGuide.setPageTransformer(false, new CustPagerTransformer(this));

    }
}
