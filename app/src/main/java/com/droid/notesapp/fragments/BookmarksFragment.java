package com.droid.notesapp.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.droid.notesapp.Adapters.RVBookmarksAdapter;
import com.droid.notesapp.Adapters.RVCategoriesAdapter;
import com.droid.notesapp.MainActivity;
import com.droid.notesapp.Models.CategoryModel;
import com.droid.notesapp.Models.LectureModel;
import com.droid.notesapp.R;

import java.util.ArrayList;


public class BookmarksFragment extends BaseFragment implements View.OnClickListener, RVBookmarksAdapter.ItemClickListener {

    private View rootView;
    RecyclerView rvCategories;
    private ArrayList<LectureModel> arrListBookmarks;// = new ArrayList<>();
    RVBookmarksAdapter rvBookmarksAdapter;

    TextView txtNoFound;

    public BookmarksFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_browse_categories, container,
                false);

        initialWork();
        initializeControls();
        setOnClickListeners();
        setRecyclerView();
        return rootView;
    }


    public void initialWork() {
        ((MainActivity) getActivity()).initialWorkForAllFragments(getActivity(),
                getActivity().getString(R.string.bookmarks), View.VISIBLE, R.mipmap.icon_menu, View.GONE, 0);
    }

    private void initializeControls() {
        txtNoFound = (TextView) rootView.findViewById(R.id.txtNoFound);
        rvCategories = (RecyclerView) rootView.findViewById(R.id.rvCategories);
    }

    public void setOnClickListeners() {
        ((MainActivity) getActivity()).llMenu1.setOnClickListener(this);
//        ((MainActivity) getActivity()).llMenu2.setOnClickListener(this);
    }

    public void setRecyclerView() {
        setData();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvCategories.setLayoutManager(mLayoutManager);

        rvBookmarksAdapter = new RVBookmarksAdapter(getActivity(), arrListBookmarks);
        rvCategories.setAdapter(rvBookmarksAdapter);
        rvBookmarksAdapter.setClickListener(this);
    }


    public void setData() {
        arrListBookmarks = new ArrayList<>();
        arrListBookmarks = ((MainActivity) getActivity()).mDatabase.getBookmarkLectures();

        if (arrListBookmarks.size() > 0) {
            txtNoFound.setVisibility(View.GONE);
        } else {
            txtNoFound.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llMenu1:

                ((MainActivity) getActivity()).openAndCloseDrawer(true);
                break;
            case R.id.llMenu2:

//                ((MainActivity) getActivity()).replaceFragment(new PostJobFragment(),getActivity());
                break;
            case R.id.tvCancel:
                dialog.dismiss();
                break;
        }
    }

    @Override
    public void onItemClickC(View view, int position) {
        if (view.getTag().equals(getActivity().getString(R.string.parent))) {

            ((MainActivity) getActivity()).replaceFragment(new PlayLectureFragment(arrListBookmarks.get(position)), getActivity());
        } else {
            dialog = ((MainActivity) getActivity()).objGlobalHelperNormal.showDialog(R.layout.dialog_stop_recording, getActivity(), true, android.R.style.Theme_Translucent_NoTitleBar);
            initializeDialogBookmark(position);
        }
    }

    Dialog dialog;

    private void initializeDialogBookmark(final int position) {
        TextView tvDialogText = dialog.findViewById(R.id.tvDialogText);
        tvDialogText.setText("Do you want to remove Bookmark?");
        dialog.findViewById(R.id.tvCancel).setOnClickListener(this);
        dialog.findViewById(R.id.tvYes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).mDatabase.setBookmark(arrListBookmarks.get(position), 0);
                setRecyclerView();
                dialog.dismiss();
            }
        });
    }
}
