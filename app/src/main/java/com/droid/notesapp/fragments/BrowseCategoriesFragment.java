package com.droid.notesapp.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.droid.notesapp.Adapters.RVCategoriesAdapter;
import com.droid.notesapp.MainActivity;
import com.droid.notesapp.Models.CategoryModel;
import com.droid.notesapp.R;
import com.droid.notesapp.helper.FrontEngine;

import java.util.ArrayList;


public class BrowseCategoriesFragment extends BaseFragment implements View.OnClickListener, RVCategoriesAdapter.ItemClickListener {

    private View rootView;
    RecyclerView rvCategories;
    private ArrayList<CategoryModel> arrListCategories;// = new ArrayList<>();
    RVCategoriesAdapter rvCategoriesAdapter;

    TextView txtNoFound;

    public BrowseCategoriesFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_browse_categories, container,
                false);

        initialWork();
        initializeControls();
        setOnClickListeners();
        setRecyclerView();
        return rootView;
    }


    public void initialWork() {
        ((MainActivity) getActivity()).initialWorkForAllFragments(getActivity(),
                getActivity().getString(R.string.browse_categories), View.VISIBLE, R.mipmap.icon_menu, View.VISIBLE, R.mipmap.add_button);
    }

    private void initializeControls() {
        rvCategories = (RecyclerView) rootView.findViewById(R.id.rvCategories);
        txtNoFound = (TextView) rootView.findViewById(R.id.txtNoFound);
    }

    public void setOnClickListeners() {
        ((MainActivity) getActivity()).llMenu1.setOnClickListener(this);
        ((MainActivity) getActivity()).llMenu2.setOnClickListener(this);
    }

    public void setRecyclerView() {
        setData();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvCategories.setLayoutManager(mLayoutManager);

        rvCategoriesAdapter = new RVCategoriesAdapter(getActivity(), arrListCategories);
        rvCategories.setAdapter(rvCategoriesAdapter);
        rvCategoriesAdapter.setClickListener(this);
    }


    public void setData() {
        arrListCategories = new ArrayList<>();
//        CategoryModel categoryModel = new CategoryModel();
////
//        categoryModel.setId(1);
//        categoryModel.setTitle("Category Name");
//
//        arrListCategories.add(categoryModel);
//        arrListCategories.add(categoryModel);
//        arrListCategories.add(categoryModel);
//        arrListCategories.add(categoryModel);
//        arrListCategories.add(categoryModel);
//        arrListCategories.add(categoryModel);
//        arrListCategories.add(categoryModel);
//        arrListCategories.add(categoryModel);

        arrListCategories = FrontEngine.getInstance().getArrListCategory(getActivity());

        if(arrListCategories.size()>0){
            txtNoFound.setVisibility(View.GONE);
        }else {
            txtNoFound.setVisibility(View.VISIBLE);
        }
    }

    public void addCategory(String categoryName){
        CategoryModel categoryModel = new CategoryModel();
////
        categoryModel.setId(1);
        categoryModel.setTitle(categoryName);
        arrListCategories.add(categoryModel);

        FrontEngine.getInstance().setArrListCategory(getActivity(),arrListCategories);
        setRecyclerView();
    }

    private void deleteCategory(int position){
        arrListCategories.remove(position);
        FrontEngine.getInstance().setArrListCategory(getActivity(),arrListCategories);
        setRecyclerView();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llMenu1:

                ((MainActivity) getActivity()).openAndCloseDrawer(true);
                break;
            case R.id.llMenu2:

//                ((MainActivity) getActivity()).replaceFragment(new PostJobFragment(),getActivity());
                dialog = ((MainActivity) getActivity()).objGlobalHelperNormal.showDialog(R.layout.dialog_add_category, getActivity(), true, android.R.style.Theme_Translucent_NoTitleBar);
                initializeAndSetClickListenerOnDialogChilds();
                break;
            case R.id.btnClose:
                dialog.dismiss();
                break;
            case R.id.tvCancel:
                dialog.dismiss();
                break;
            case R.id.tvSave:

                if(etCategoryName.getText().toString().equals("")){

                }else {
                    addCategory(etCategoryName.getText().toString());
                }

                dialog.dismiss();

                break;
        }
    }

    Dialog dialog;
    EditText etCategoryName;

    private void initializeAndSetClickListenerOnDialogChilds() {
        etCategoryName = dialog.findViewById(R.id.etCategoryName);

        dialog.findViewById(R.id.btnClose).setOnClickListener(this);
        dialog.findViewById(R.id.tvCancel).setOnClickListener(this);
        dialog.findViewById(R.id.tvSave).setOnClickListener(this);
    }

    private void initializeAndSetClickListenerOnDialogChildsx(final int position) {
        TextView tvDialogText = dialog.findViewById(R.id.tvDialogText);
        tvDialogText.setText("Do you really want to delete this Category?");
        dialog.findViewById(R.id.tvCancel).setOnClickListener(this);
        dialog.findViewById(R.id.tvYes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteCategory(position);
                dialog.dismiss();
            }
        });
    }


    @Override
    public void onItemClickC(View view, int position) {
        if (view.getTag().equals("main")) {

            ((MainActivity) getActivity()).replaceFragment(new CategoryLecturesFragment(arrListCategories.get(position)), getActivity());
        } else if (view.getTag().equals("del")) {
            dialog = ((MainActivity) getActivity()).objGlobalHelperNormal.showDialog(R.layout.dialog_stop_recording, getActivity(), true, android.R.style.Theme_Translucent_NoTitleBar);
            initializeAndSetClickListenerOnDialogChildsx(position);
        }
    }
}
