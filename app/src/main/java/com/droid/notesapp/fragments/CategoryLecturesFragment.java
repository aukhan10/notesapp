package com.droid.notesapp.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.droid.notesapp.Adapters.RVSearchLecturesAdapter;
import com.droid.notesapp.MainActivity;
import com.droid.notesapp.Models.CategoryModel;
import com.droid.notesapp.Models.LectureModel;
import com.droid.notesapp.R;

import java.util.ArrayList;


public class CategoryLecturesFragment extends BaseFragment implements View.OnClickListener, RVSearchLecturesAdapter.ItemClickListener {

    private View rootView;
    RecyclerView rvLectures;
    private ArrayList<LectureModel> arrListLectures;// = new ArrayList<>();
    RVSearchLecturesAdapter rvSearchLecturesAdapter;
    private CategoryModel categoryModel;

    TextView txtNoFound;

    public CategoryLecturesFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public CategoryLecturesFragment(CategoryModel categoryModel) {
        this.categoryModel = categoryModel;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_category_lectures, container,
                false);

        initialWork();
        initializeControls();
        setOnClickListeners();
        setRecyclerView();
        return rootView;
    }


    public void initialWork() {
        ((MainActivity) getActivity()).initialWorkForAllFragments(getActivity(),
                getActivity().getString(R.string.categories_name), View.VISIBLE, R.mipmap.arrow_back, View.GONE, 0);
    }

    private void initializeControls() {
        rvLectures = (RecyclerView) rootView.findViewById(R.id.rvCategories);
        txtNoFound = (TextView) rootView.findViewById(R.id.txtNoFound);
    }

    public void setOnClickListeners() {
        ((MainActivity) getActivity()).llMenu1.setOnClickListener(this);
//        ((MainActivity) getActivity()).llMenu2.setOnClickListener(this);
    }

    public void setRecyclerView() {
        setData();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvLectures.setLayoutManager(mLayoutManager);

        rvSearchLecturesAdapter = new RVSearchLecturesAdapter(getActivity(), arrListLectures);
        rvLectures.setAdapter(rvSearchLecturesAdapter);
        rvSearchLecturesAdapter.setClickListener(this);
    }


    public void setData() {
        arrListLectures = new ArrayList<>();
        arrListLectures = ((MainActivity) getActivity()).mDatabase.getCategoryLectures(categoryModel.getTitle());

        if(arrListLectures.size()>0){
            txtNoFound.setVisibility(View.GONE);
        }else {
            txtNoFound.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llMenu1:

                ((MainActivity) getActivity()).onBackPressed();
                break;
            case R.id.llMenu2:

//                ((MainActivity) getActivity()).replaceFragment(new PostJobFragment(),getActivity());
                break;
        }
    }

    @Override
    public void onItemClickC(View view, int position) {
        ((MainActivity) getActivity()).replaceFragment(new PlayLectureFragment(arrListLectures.get(position)), getActivity());
    }
}
