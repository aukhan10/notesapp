package com.droid.notesapp.fragments;

import android.os.Bundle;
import android.text.Html;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.droid.notesapp.MainActivity;
import com.droid.notesapp.R;

public class GetReferencesFragment extends BaseFragment implements View.OnClickListener  {

    private View rootView;
    TextView txtInfo;

    public GetReferencesFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_get_references, container,
                false);

        initialWork();
        initializeControls();
        setOnClickListeners();
        setData();
        return rootView;
    }



    public void initialWork() {
        ((MainActivity) getActivity()).initialWorkForAllFragments(getActivity(),
                getActivity().getString(R.string.get_references), View.VISIBLE, R.mipmap.arrow_back, View.GONE, 0);
    }


    private void initializeControls() {
        txtInfo = (TextView) rootView.findViewById(R.id.txtInfo);
    }

    public void setOnClickListeners() {
        ((MainActivity) getActivity()).llMenu1.setOnClickListener(this);
//        ((MainActivity) getActivity()).llMenu2.setOnClickListener(this);
    }

    public void setData(){
//        txtInfo.setText(Html.fromHtml(data));
        txtInfo.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                // Remove the "select all" option
                menu.removeItem(android.R.id.selectAll);
                // Remove the "cut" option
                menu.removeItem(android.R.id.cut);
                // Remove the "copy all" option
                menu.removeItem(android.R.id.copy);
                menu.removeItem(android.R.id.shareText);
                return true;
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                // Called when action mode is first created. The menu supplied
                // will be used to generate action buttons for the action mode

                // Here is an example MenuItem
                menu.add(0, 786, 0, "Look up for references").setIcon(R.mipmap.search_button);
                return true;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                // Called when an action mode is about to be exited and
                // destroyed
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    case 786:
                        int min = 0;
                        int max = txtInfo.getText().length();
                        if (txtInfo.isFocused()) {
                            final int selStart = txtInfo.getSelectionStart();
                            final int selEnd = txtInfo.getSelectionEnd();

                            min = Math.max(0, Math.min(selStart, selEnd));
                            max = Math.max(0, Math.max(selStart, selEnd));
                        }
                        // Perform your definition lookup with the selected text
                        final CharSequence selectedText = txtInfo.getText().subSequence(min, max);
                        // Finish and close the ActionMode
                        search(selectedText);
                        mode.finish();
                        return true;
                    default:
                        break;
                }
                return false;
            }
        });
    }

    private void search(CharSequence selectedText){
        Toast.makeText(getActivity(), ""+selectedText, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llMenu1:

                ((MainActivity) getActivity()).onBackPressed();
                break;
            case R.id.llMenu2:

//                ((MainActivity) getActivity()).replaceFragment(new PostJobFragment(),getActivity());
                break;
        }
    }

}
