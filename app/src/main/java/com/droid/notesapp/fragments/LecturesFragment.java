package com.droid.notesapp.fragments;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.droid.notesapp.Adapters.RVBookmarksAdapter;
import com.droid.notesapp.Adapters.RVLecturesAdapter;
import com.droid.notesapp.Adapters.UserGuideAdapter;
import com.droid.notesapp.DBHelper;
import com.droid.notesapp.MainActivity;
import com.droid.notesapp.Models.LectureModel;
import com.droid.notesapp.R;
import com.droid.notesapp.animations.CustPagerTransformer;
import com.droid.notesapp.helper.FixedSpeedScroller;

import java.lang.reflect.Field;
import java.util.ArrayList;


public class LecturesFragment extends BaseFragment implements View.OnClickListener, RVLecturesAdapter.ItemClickListener {

    private View rootView;
    RecyclerView rvLectures;
    private ArrayList<LectureModel> arrListLectures;// = new ArrayList<>();
    private ArrayList<String> arrListDates;// = new ArrayList<>();
    RVLecturesAdapter rvLecturesAdapter;
    private ViewPager viewPagerDates;
    UserGuideAdapter userGuideAdapter;

    ImageView nextPager, backPager;

    TextView txtNoFound;

    public LecturesFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_lectures, container,
                false);

//        mDatabase = new DBHelper(getActivity());
        initialWork();
        initializeControls();
        setOnClickListeners();
        setData();
        setUpUserGuideViewPagerWithAdapter();
        setPageScrollSpeedAndAnimation();
        return rootView;
    }


    public void initialWork() {
        ((MainActivity) getActivity()).initialWorkForAllFragments(getActivity(),
                getActivity().getString(R.string.lectures), View.VISIBLE, R.mipmap.icon_menu, View.VISIBLE, R.mipmap.search_button);
    }

    private void initializeControls() {
        txtNoFound = (TextView) rootView.findViewById(R.id.txtNoFound);
        rvLectures = (RecyclerView) rootView.findViewById(R.id.rvCategories);
        nextPager = (ImageView) rootView.findViewById(R.id.nextPager);
        backPager = (ImageView) rootView.findViewById(R.id.backPager);
    }

    public void setOnClickListeners() {
        ((MainActivity) getActivity()).llMenu1.setOnClickListener(this);
        ((MainActivity) getActivity()).llMenu2.setOnClickListener(this);
        nextPager.setOnClickListener(this);
        backPager.setOnClickListener(this);
    }

    private void setUpUserGuideViewPagerWithAdapter() {
        viewPagerDates = rootView.findViewById(R.id.viewPagerDates);
        userGuideAdapter = new UserGuideAdapter(getActivity(), arrListDates);
        viewPagerDates.setAdapter(userGuideAdapter);
        viewPagerDates.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Log.e("Position", "" + position);
                Log.e("Date", "" + userGuideAdapter.getItem(position));
                setDataLecture(userGuideAdapter.getItem(position));
//                View view = viewPagerDates.getChildAt(position);
//                TextView txtDate = view.findViewById(R.id.txtDate);
//                txtDate.setText("aa");
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        int x = (arrListDates.size()) / 2;

        viewPagerDates.setCurrentItem(x, true);
        if(x==0){
            setDataLecture(userGuideAdapter.getItem(x));
        }
    }

    private void setPageScrollSpeedAndAnimation() {
        Field mScroller = null;
        // this work of scroller is done to slow the speed of scroll...
        try {
            mScroller = ViewPager.class.getDeclaredField("mScroller");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        mScroller.setAccessible(true);
        FixedSpeedScroller scroller = new FixedSpeedScroller(viewPagerDates.getContext(), 1000);

        try {
            mScroller.set(viewPagerDates, scroller);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        viewPagerDates.setPageTransformer(false, new CustPagerTransformer(getActivity()));

    }

    public void setRecyclerView() {
//        setData();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvLectures.setLayoutManager(mLayoutManager);

        rvLecturesAdapter = new RVLecturesAdapter(getActivity(), arrListLectures);
        rvLectures.setAdapter(rvLecturesAdapter);
        rvLecturesAdapter.setClickListener(this);
    }


    public void setData() {

        arrListDates = new ArrayList<>();
        arrListDates = ((MainActivity) getActivity()).mDatabase.getDates();


    }

    public void setDataLecture(String item) {
        arrListLectures = new ArrayList<>();
        arrListLectures = ((MainActivity) getActivity()).mDatabase.getDateLectures(item);
        setRecyclerView();
        if (arrListLectures.size() > 0) {
            txtNoFound.setVisibility(View.GONE);
        } else {
            txtNoFound.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llMenu1:

                ((MainActivity) getActivity()).openAndCloseDrawer(true);
                break;
            case R.id.llMenu2:

                ((MainActivity) getActivity()).replaceFragment(new SearchLecturesFragment(), getActivity());
                break;
            case R.id.nextPager:

                changeDate(true);
                break;
            case R.id.backPager:

                changeDate(false);
                break;
        }
    }

    private void changeDate(boolean b) {
        int size = arrListDates.size();
        int curr = viewPagerDates.getCurrentItem();
        if (b) {
            curr++;
        } else {
            curr--;
        }
        if (curr == size) {
            curr = 0;
        }
        if (curr < 0) {
            curr = size - 1;
        }
        viewPagerDates.setCurrentItem(curr, true);
    }

    @Override
    public void onItemClickC(View view, int position) {
        ((MainActivity) getActivity()).replaceFragment(new PlayLectureFragment(arrListLectures.get(position)), getActivity());
    }
}
