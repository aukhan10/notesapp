package com.droid.notesapp.fragments;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.droid.notesapp.Adapters.RVPlansAdapter;
import com.droid.notesapp.MainActivity;
import com.droid.notesapp.Models.CategoryModel;
import com.droid.notesapp.R;

import java.util.ArrayList;


public class PlansFragment extends BaseFragment implements View.OnClickListener,RVPlansAdapter.ItemClickListener {

    private View rootView;
    RecyclerView rvPlans;
    private ArrayList<CategoryModel> arrListCategories;// = new ArrayList<>();
    RVPlansAdapter rvPlansAdapter;

    public PlansFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_plans, container,
                false);

        initialWork();
        initializeControls();
        setOnClickListeners();
        setRecyclerView();
        return rootView;
    }



    public void initialWork() {
        ((MainActivity) getActivity()).initialWorkForAllFragments(getActivity(),
                getActivity().getString(R.string.premiumplans), View.VISIBLE, R.mipmap.arrow_back, View.GONE, 0);
    }

    private void initializeControls() {
        rvPlans = (RecyclerView) rootView.findViewById(R.id.rvPlans);
    }

    public void setOnClickListeners() {
        ((MainActivity) getActivity()).llMenu1.setOnClickListener(this);
//        ((MainActivity) getActivity()).llMenu2.setOnClickListener(this);
    }

    public void setRecyclerView() {
        setData();
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        rvPlans.setLayoutManager(mLayoutManager);

        rvPlansAdapter = new RVPlansAdapter(getActivity(), arrListCategories);
        rvPlans.setAdapter(rvPlansAdapter);
        rvPlansAdapter.setClickListener(this);
    }


    public void setData() {
        arrListCategories = new ArrayList<>();
        CategoryModel categoryModel = new CategoryModel();
//
        categoryModel.setId(1);
        categoryModel.setTitle("Monthly");
        arrListCategories.add(categoryModel);
        categoryModel.setTitle("Quarterly");
        arrListCategories.add(categoryModel);
        categoryModel.setTitle("Half Yearly");
        arrListCategories.add(categoryModel);
        categoryModel.setTitle("Yearly");
        arrListCategories.add(categoryModel);
//
//        arrListTopStatements = FrontEngine.getInstance().getArrListTopStatements();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llMenu1:

                ((MainActivity) getActivity()).onBackPressed();
                break;
            case R.id.llMenu2:

//                ((MainActivity) getActivity()).replaceFragment(new PostJobFragment(),getActivity());
                break;
        }
    }

    @Override
    public void onItemClickC(View view, int position) {
//        ((MainActivity) getActivity()).replaceFragment(new CategoryLecturesFragment(), getActivity());
    }

}
