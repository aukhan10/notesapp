package com.droid.notesapp.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.chibde.visualizer.LineBarVisualizer;
import com.droid.notesapp.MainActivity;
import com.droid.notesapp.Models.LectureModel;
import com.droid.notesapp.R;
import com.droid.notesapp.helper.FrontEngine;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;


public class PlayLectureFragment extends BaseFragment implements View.OnClickListener {

    private View rootView;
    TextView txtLectureCategory, txtBookmark, txtDateTime, txtDelete;
    TextView txtGetRefrence, txtGoPremium;
    LineBarVisualizer lineBarVisualizer;
    LectureModel lectureModel;
    private Handler mHandler = new Handler();

    private MediaPlayer mediaPlayer = null;
    SeekBar seekBar;
    ImageView revBtn, playBtn, frwdBtn;
    TextView tvCurrentProgress, tvFileLength;

    //stores whether or not the mediaplayer is currently playing audio
    private boolean isPlaying = false;

    //stores minutes and seconds of the length of the file.
    long minutes = 0;
    long seconds = 0;

    public PlayLectureFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public PlayLectureFragment(LectureModel lectureModel) {
        this.lectureModel = lectureModel;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_play_lecture, container,
                false);


        initializeControls();
        setOnClickListeners();
        setData();
        return rootView;
    }


    public void initialWork(String title) {
        ((MainActivity) getActivity()).initialWorkForAllFragments(getActivity(),
                title, View.VISIBLE, R.mipmap.arrow_back, View.GONE, 0);
    }

    private void initializeControls() {
        txtLectureCategory = (TextView) rootView.findViewById(R.id.txtLectureCategory);
        txtBookmark = (TextView) rootView.findViewById(R.id.txtBookmark);
        txtDateTime = (TextView) rootView.findViewById(R.id.txtDateTime);
        txtDelete = (TextView) rootView.findViewById(R.id.txtDelete);

        txtGoPremium = (TextView) rootView.findViewById(R.id.txtGoPremium);
        txtGetRefrence = (TextView) rootView.findViewById(R.id.txtGetRefrence);
        lineBarVisualizer = rootView.findViewById(R.id.visualizer);

        seekBar = (SeekBar) rootView.findViewById(R.id.seekBar);
        revBtn = (ImageView) rootView.findViewById(R.id.revBtn);
        playBtn = (ImageView) rootView.findViewById(R.id.playBtn);
        frwdBtn = (ImageView) rootView.findViewById(R.id.frwdBtn);

        tvCurrentProgress = (TextView) rootView.findViewById(R.id.tvCurrentProgress);
        tvFileLength = (TextView) rootView.findViewById(R.id.tvFileLength);
    }

    public void setOnClickListeners() {
        ((MainActivity) getActivity()).llMenu1.setOnClickListener(this);
//        ((MainActivity) getActivity()).llMenu2.setOnClickListener(this);
        txtBookmark.setOnClickListener(this);
        txtDelete.setOnClickListener(this);
        txtGoPremium.setOnClickListener(this);
        txtGetRefrence.setOnClickListener(this);

        revBtn.setOnClickListener(this);
        playBtn.setOnClickListener(this);
        frwdBtn.setOnClickListener(this);
    }

    public void setData() {
        initialWork(lectureModel.getTitle());
        txtLectureCategory.setText(lectureModel.getCategory());

        if (lectureModel.getBookmark() == 1) {
            txtBookmark.setText(getActivity().getString(R.string.bookmarked));
        } else {
            txtBookmark.setText(getActivity().getString(R.string.bookmark));
        }

        String date, timeStart, timeEnd;
        date = FrontEngine.getInstance().secsToFullDate2Format(lectureModel.getTimestart());
        timeStart = FrontEngine.getInstance().secsToTimeFormat(lectureModel.getTimestart());
        timeEnd = FrontEngine.getInstance().secsToTimeFormat(lectureModel.getTimeend());

        txtDateTime.setText(date + "  " + timeStart + " - " + timeEnd);

        long itemDuration = lectureModel.getLength();
        minutes = TimeUnit.MILLISECONDS.toMinutes(itemDuration);
        seconds = TimeUnit.MILLISECONDS.toSeconds(itemDuration)
                - TimeUnit.MINUTES.toSeconds(minutes);

        tvFileLength.setText(String.format("%02d:%02d", minutes, seconds));

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (mediaPlayer != null && fromUser) {
                    mediaPlayer.seekTo(progress);
                    mHandler.removeCallbacks(mRunnable);

                    long minutes = TimeUnit.MILLISECONDS.toMinutes(mediaPlayer.getCurrentPosition());
                    long seconds = TimeUnit.MILLISECONDS.toSeconds(mediaPlayer.getCurrentPosition())
                            - TimeUnit.MINUTES.toSeconds(minutes);
                    tvCurrentProgress.setText(String.format("%02d:%02d", minutes, seconds));

                    updateSeekBar();

                } else if (mediaPlayer == null && fromUser) {
                    prepareMediaPlayerFromPoint(progress);
                    updateSeekBar();
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                if (mediaPlayer != null) {
                    // remove message Handler from updating progress bar
                    mHandler.removeCallbacks(mRunnable);
                }
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (mediaPlayer != null) {
                    mHandler.removeCallbacks(mRunnable);
                    mediaPlayer.seekTo(seekBar.getProgress());

                    long minutes = TimeUnit.MILLISECONDS.toMinutes(mediaPlayer.getCurrentPosition());
                    long seconds = TimeUnit.MILLISECONDS.toSeconds(mediaPlayer.getCurrentPosition())
                            - TimeUnit.MINUTES.toSeconds(minutes);
                    tvCurrentProgress.setText(String.format("%02d:%02d", minutes, seconds));
                    updateSeekBar();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llMenu1:

                ((MainActivity) getActivity()).onBackPressed();
                break;
            case R.id.llMenu2:

//                ((MainActivity) getActivity()).replaceFragment(new PostJobFragment(),getActivity());
                break;
            case R.id.txtGetRefrence:

                ((MainActivity) getActivity()).replaceFragment(new GetReferencesFragment(), getActivity());
                break;
            case R.id.txtGoPremium:

                ((MainActivity) getActivity()).replaceFragment(new PlansFragment(), getActivity());
                break;
            case R.id.txtBookmark:
                if (lectureModel.getBookmark() == 1) {
                    dialog = ((MainActivity) getActivity()).objGlobalHelperNormal.showDialog(R.layout.dialog_stop_recording, getActivity(), true, android.R.style.Theme_Translucent_NoTitleBar);
                    initializeDialogBookmark();
                } else {
                    setBookmark();
                }
                break;
            case R.id.txtDelete:

                dialog = ((MainActivity) getActivity()).objGlobalHelperNormal.showDialog(R.layout.dialog_stop_recording, getActivity(), true, android.R.style.Theme_Translucent_NoTitleBar);
                initializeAndSetClickListenerOnDialogChilds();
                break;
            case R.id.tvCancel:
                dialog.dismiss();
                break;
            case R.id.tvYes:
                dialog.dismiss();
                deleteLecture();

                break;

            case R.id.revBtn:

                break;
            case R.id.playBtn:
                onPlay(isPlaying);
                isPlaying = !isPlaying;
                break;
            case R.id.frwdBtn:

                break;
        }
    }

    Dialog dialog;

    private void initializeAndSetClickListenerOnDialogChilds() {
        TextView tvDialogText = dialog.findViewById(R.id.tvDialogText);
        tvDialogText.setText("Do you really want to delete the lecture?");
        dialog.findViewById(R.id.tvCancel).setOnClickListener(this);
        dialog.findViewById(R.id.tvYes).setOnClickListener(this);
    }

    private void initializeDialogBookmark() {
        TextView tvDialogText = dialog.findViewById(R.id.tvDialogText);
        tvDialogText.setText("Do you want to remove Bookmark?");
        dialog.findViewById(R.id.tvCancel).setOnClickListener(this);
        dialog.findViewById(R.id.tvYes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBookmark();
                dialog.dismiss();
            }
        });
    }

    private void deleteLecture() {
        //remove item from database, recyclerview and storage

        //delete file from storage
        File file = new File(lectureModel.getFilePath());
        file.delete();

        Toast.makeText(
                getActivity(),
                String.format(
                        getActivity().getString(R.string.toast_file_delete),
                        lectureModel.getName()
                ),
                Toast.LENGTH_SHORT
        ).show();

//        mDatabase.removeItemWithId(lectureModel.getId());
//        notifyItemRemoved(position);

        ((MainActivity) getActivity()).mDatabase.removeItemWithId(lectureModel.getId());
        ((MainActivity) getActivity()).onBackPressed();
    }

    private void setBookmark() {
        //add/remove bookmark
        if (lectureModel.getBookmark() == 1) {

            ((MainActivity) getActivity()).mDatabase.setBookmark(lectureModel, 0);
            lectureModel.setBookmark(0);
            txtBookmark.setText(getActivity().getString(R.string.bookmark));
        } else {
            ((MainActivity) getActivity()).mDatabase.setBookmark(lectureModel, 1);
            lectureModel.setBookmark(1);
            txtBookmark.setText(getActivity().getString(R.string.bookmarked));
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (mediaPlayer != null) {
            stopPlaying();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mediaPlayer != null) {
            stopPlaying();
        }
    }

    // Play start/stop
    private void onPlay(boolean isPlaying) {
        if (!isPlaying) {
            //currently MediaPlayer is not playing audio
            if (mediaPlayer == null) {
                startPlaying(); //start from beginning
            } else {
                resumePlaying(); //resume the currently paused MediaPlayer
            }

        } else {
            //pause the MediaPlayer
            pausePlaying();
        }
    }


    public void startPlaying() {
        playBtn.setImageResource(R.mipmap.pause2_button);
        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource("" + lectureModel.getFilePath());
            mediaPlayer.prepare();
            seekBar.setMax(mediaPlayer.getDuration());
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mediaPlayer.start();
                }
            });
            // set custom color to the line.
            lineBarVisualizer.setColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
            // define custom number of bars you want in the visualizer between (10 - 256).
            lineBarVisualizer.setDensity(180);
            // Set you media player to the visualizer.
            lineBarVisualizer.setPlayer(mediaPlayer.getAudioSessionId());

        } catch (IOException e) {
            e.printStackTrace();
        }
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
//                mediaPlayer.release();
                stopPlaying();
            }
        });

        updateSeekBar();

        //keep screen on while playing audio
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

    }

    private void prepareMediaPlayerFromPoint(int progress) {
        //set mediaPlayer to start from middle of the audio file

        mediaPlayer = new MediaPlayer();

        try {
            mediaPlayer.setDataSource(lectureModel.getFilePath());
            mediaPlayer.prepare();
            seekBar.setMax(mediaPlayer.getDuration());
            mediaPlayer.seekTo(progress);

            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    stopPlaying();
                }
            });

        } catch (IOException e) {
            Log.e("PlaybackFragment", "prepare() failed");
        }

        //keep screen on while playing audio
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    private void pausePlaying() {
        playBtn.setImageResource(R.mipmap.play2_button);
        mHandler.removeCallbacks(mRunnable);
        mediaPlayer.pause();
    }

    private void resumePlaying() {
        playBtn.setImageResource(R.mipmap.pause2_button);
        mHandler.removeCallbacks(mRunnable);
        mediaPlayer.start();
        updateSeekBar();
    }

    private void stopPlaying() {
        playBtn.setImageResource(R.mipmap.play2_button);
        mHandler.removeCallbacks(mRunnable);
        mediaPlayer.stop();
        mediaPlayer.reset();
        mediaPlayer.release();
        mediaPlayer = null;

        seekBar.setProgress(seekBar.getMax());
        isPlaying = !isPlaying;

        tvCurrentProgress.setText(tvFileLength.getText());
        seekBar.setProgress(seekBar.getMax());

        //allow the screen to turn off again once audio is finished playing
        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    //updating seekBar
    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            if (mediaPlayer != null) {

                int mCurrentPosition = mediaPlayer.getCurrentPosition();
                seekBar.setProgress(mCurrentPosition);

                long minutes = TimeUnit.MILLISECONDS.toMinutes(mCurrentPosition);
                long seconds = TimeUnit.MILLISECONDS.toSeconds(mCurrentPosition)
                        - TimeUnit.MINUTES.toSeconds(minutes);
                tvCurrentProgress.setText(String.format("%02d:%02d", minutes, seconds));

                updateSeekBar();
            }
        }
    };

    private void updateSeekBar() {
        mHandler.postDelayed(mRunnable, 1000);
    }


}
