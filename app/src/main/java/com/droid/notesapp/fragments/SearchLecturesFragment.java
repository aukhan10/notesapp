package com.droid.notesapp.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.droid.notesapp.Adapters.RVLecturesAdapter;
import com.droid.notesapp.Adapters.RVSearchLecturesAdapter;
import com.droid.notesapp.DBHelper;
import com.droid.notesapp.MainActivity;
import com.droid.notesapp.Models.LectureModel;
import com.droid.notesapp.R;

import java.util.ArrayList;


public class SearchLecturesFragment extends BaseFragment implements View.OnClickListener, RVSearchLecturesAdapter.ItemClickListener {

    private View rootView;
    RecyclerView rvLectures;
    private ArrayList<LectureModel> arrListLectures;// = new ArrayList<>();
    RVSearchLecturesAdapter rvSearchLecturesAdapter;

    EditText etSearch;
    ImageView btnSearch;
    TextView tvSearchCount;

    TextView txtNoFound;

    public SearchLecturesFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_search_lectures, container,
                false);

        initialWork();
        initializeControls();
        setOnClickListeners();
//        setRecyclerView();
        return rootView;
    }


    public void initialWork() {
        ((MainActivity) getActivity()).initialWorkForAllFragments(getActivity(),
                getActivity().getString(R.string.search_lectures), View.VISIBLE, R.mipmap.arrow_back, View.GONE, 0);
    }

    private void initializeControls() {
        txtNoFound = (TextView) rootView.findViewById(R.id.txtNoFound);
        rvLectures = (RecyclerView) rootView.findViewById(R.id.rvCategories);
        etSearch = (EditText) rootView.findViewById(R.id.etSearch);
        btnSearch = (ImageView) rootView.findViewById(R.id.btnSearch);
        tvSearchCount = (TextView) rootView.findViewById(R.id.tvSearchCount);
    }

    public void setOnClickListeners() {
        ((MainActivity) getActivity()).llMenu1.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
    }

    public void setRecyclerView() {
//        setData();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvLectures.setLayoutManager(mLayoutManager);

        rvSearchLecturesAdapter = new RVSearchLecturesAdapter(getActivity(), arrListLectures);
        rvLectures.setAdapter(rvSearchLecturesAdapter);
        rvSearchLecturesAdapter.setClickListener(this);
    }


    public void setData() {
        arrListLectures = new ArrayList<>();
        arrListLectures = ((MainActivity) getActivity()).mDatabase.getSearchLectures(etSearch.getText().toString());
        if(arrListLectures.size()>0){
            txtNoFound.setVisibility(View.GONE);
        }else {
            txtNoFound.setVisibility(View.VISIBLE);
        }
        setRecyclerView();
        tvSearchCount.setText(arrListLectures.size()+" results found");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llMenu1:

                ((MainActivity) getActivity()).onBackPressed();
                break;
            case R.id.btnSearch:
                ((MainActivity) getActivity()).objGlobalHelperNormal.hideSoftKeyboard(getActivity());
                setData();
                break;
        }
    }

    @Override
    public void onItemClickC(View view, int position) {
        ((MainActivity) getActivity()).replaceFragment(new PlayLectureFragment(arrListLectures.get(position)), getActivity());
    }
}
