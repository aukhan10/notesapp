package com.droid.notesapp.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chibde.visualizer.LineBarVisualizer;
import com.droid.notesapp.MainActivity;
import com.droid.notesapp.Models.CategoryModel;
import com.droid.notesapp.R;
import com.droid.notesapp.RecordingService;
import com.droid.notesapp.helper.FrontEngine;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


public class StartLectureFragment extends BaseFragment implements View.OnClickListener {

    private View rootView;

    private Chronometer mChronometer = null;
    ImageView mRecordButton, mStopButton;
    LineBarVisualizer lineBarVisualizer;

    long timeWhenPaused = 0; //stores time when user clicks pause button
    private boolean mStartRecording = true;

    AutoCompleteTextView actSelectCategory;
    EditText etLectureTitle;
    TextView tvDate, tvTime;

    public StartLectureFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_start_lecture, container,
                false);

        initialWork();
        initializeControls();
        setOnClickListeners();
        return rootView;
    }


    public void initialWork() {
        ((MainActivity) getActivity()).initialWorkForAllFragments(getActivity(),
                getActivity().getString(R.string.start_lecture), View.VISIBLE, R.mipmap.icon_menu, View.GONE, 0);
    }

    public void initialWorkx() {
        ((MainActivity) getActivity()).initialWorkForAllFragments(getActivity(),
                getActivity().getString(R.string.recording_lecture), View.VISIBLE, R.mipmap.icon_menu, View.GONE, 0);
    }

    private void initializeControls() {
        mChronometer = (Chronometer) rootView.findViewById(R.id.chronometer);
        mRecordButton = (ImageView) rootView.findViewById(R.id.btnRecord);
        mStopButton = (ImageView) rootView.findViewById(R.id.btnStop);
        lineBarVisualizer = rootView.findViewById(R.id.visualizer);
        etLectureTitle = (EditText) rootView.findViewById(R.id.etLectureTitle);
        actSelectCategory = (AutoCompleteTextView) rootView.findViewById(R.id.actSelectCategory);

        tvDate = (TextView) rootView.findViewById(R.id.tvDate);
        tvTime = (TextView) rootView.findViewById(R.id.tvTime);
        setData();
    }

    private void setData() {
        tvDate.setText(FrontEngine.getInstance().secsToFullDateFormat(System.currentTimeMillis()));
        tvTime.setText(FrontEngine.getInstance().secsToTimeFormat(System.currentTimeMillis()));
    }

    private void resetData() {
        etLectureTitle.setText("");
        actSelectCategory.setText("");
    }

    public void setOnClickListeners() {
        ((MainActivity) getActivity()).llMenu1.setOnClickListener(this);
//        ((MainActivity) getActivity()).llMenu2.setOnClickListener(this);
        mRecordButton.setOnClickListener(this);
        mStopButton.setOnClickListener(this);
        actSelectCategory.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llMenu1:

                ((MainActivity) getActivity()).openAndCloseDrawer(true);
                break;
            case R.id.llMenu2:

//                ((MainActivity) getActivity()).replaceFragment(new PostJobFragment(),getActivity());
                break;
            case R.id.btnRecord:
                if (checkValidation()) {

                    onRecord(mStartRecording);
                    mStartRecording = !mStartRecording;
                }

                break;
            case R.id.btnStop:


                dialog = ((MainActivity) getActivity()).objGlobalHelperNormal.showDialog(R.layout.dialog_stop_recording, getActivity(), true, android.R.style.Theme_Translucent_NoTitleBar);
                initializeAndSetClickListenerOnDialogChilds();
                break;
            case R.id.tvCancel:
                dialog.dismiss();
//                ((MainActivity) getActivity()).replaceFragment(new PostJobFragment(),getActivity());
                break;
            case R.id.tvYes:
                dialog.dismiss();


                onStopRecording();
//                ((MainActivity) getActivity()).replaceFragment(new PlayLectureFragment(), getActivity());
                break;
            case R.id.actSelectCategory:
                //TODO implement
                loadCategoriesDataAndCheck();
                actSelectCategory.showDropDown();
                break;
        }
    }

    private boolean checkValidation() {
        boolean ret = true;
        if (!((MainActivity) getActivity()).objValidation.hasText(etLectureTitle)) {
            ret = false;
        } else if (!((MainActivity) getActivity()).objValidation.hasText(actSelectCategory)) {
            ret = false;
        }

        return ret; // todo temp true
    }

    public void loadCategoriesDataAndCheck() {
//        final String[] data = {"Category Name", "Category Name", "Category Name", "Category Name", "Category Name", "Category Name"};
        final String[] data = getCategories();
        ArrayAdapter<String> adapterchooseUserTypet = new ArrayAdapter<String>(getActivity(), R.layout.menu_item2, data);
        actSelectCategory.setAdapter(adapterchooseUserTypet);
        actSelectCategory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                catPosition = position;

            }
        });
    }

    private String[] getCategories() {
        ArrayList<CategoryModel> arrListCategory = new ArrayList<>();
        arrListCategory = FrontEngine.getInstance().getArrListCategory(getActivity());
        String[] data = new String[arrListCategory.size()];
        for (int i = 0; i < arrListCategory.size(); i++) {
            data[i] = arrListCategory.get(i).getTitle();
        }
        return data;
    }

    Dialog dialog;

    private void initializeAndSetClickListenerOnDialogChilds() {
        dialog.findViewById(R.id.tvCancel).setOnClickListener(this);
        dialog.findViewById(R.id.tvYes).setOnClickListener(this);
    }

    Intent intent;

    // Recording Start/Pause
    private void onRecord(boolean start) {
        initialWorkx();
        intent = new Intent(getActivity(), RecordingService.class);
        intent.putExtra(getString(R.string.lecturetitle), "" + etLectureTitle.getText().toString());
        intent.putExtra(getString(R.string.lecturecategory), "" + actSelectCategory.getText().toString());

        if (start) {
            // start recording
//            startRecording();
            mRecordButton.setImageResource(R.mipmap.pause_button);
//            mPauseButton.setVisibility(View.VISIBLE);
//            Toast.makeText(getActivity(),R.string.toast_recording_start,Toast.LENGTH_SHORT).show();
            File folder = new File(Environment.getExternalStorageDirectory() + "/NotesApp");
            if (!folder.exists()) {
                //folder /SoundRecorder doesn't exist, create the folder
                folder.mkdir();
            }

            //start Chronometer
            if (timeWhenPaused != 0) {
                mChronometer.setBase(SystemClock.elapsedRealtime() + timeWhenPaused);
            } else {
                mChronometer.setBase(SystemClock.elapsedRealtime());

            }
            mChronometer.start();

            //start RecordingService
            getActivity().startService(intent);
            //keep screen on while recording
            getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

//            mRecordingPrompt.setText(getString(R.string.record_in_progress) + ".");
//            mRecordPromptCount++;

        } else {
            //pause recording
            mRecordButton.setImageResource(R.mipmap.record_button);
            timeWhenPaused = mChronometer.getBase() - SystemClock.elapsedRealtime();
            mChronometer.stop();
        }
    }

    // Recording Stop
    private void onStopRecording() {
        initialWork();
        intent = new Intent(getActivity(), RecordingService.class);
        //stop recording
//        stopRecording();
        mRecordButton.setImageResource(R.mipmap.record_button);
//            mPauseButton.setVisibility(View.GONE);
        mChronometer.stop();
        mChronometer.setBase(SystemClock.elapsedRealtime());
        timeWhenPaused = 0;
//            mRecordingPrompt.setText(getString(R.string.record_prompt));

        getActivity().stopService(intent);
        //allow the screen to turn off again once recording is finished
        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        resetData();
    }

//    public void startRecording() {
////        isRecording = true;
//        MediaRecorderReady();
//
//        try {
//            mediaRecorder.prepare();
//            mediaRecorder.start();
//
//            // set custom color to the line.
//            lineBarVisualizer.setColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
////
////// define custom number of bars you want in the visualizer between (10 - 256).
//            lineBarVisualizer.setDensity(180);
////
////// Set you media player to the visualizer.
//            lineBarVisualizer.setPlayer(mediaRecorder.getMaxAmplitude());
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public void stopRecording() {
////        isRecording = false;
//        mediaRecorder.stop();
//        mediaRecorder.release();
//    }
//
//    MediaRecorder mediaRecorder = new MediaRecorder();
//
//    public void MediaRecorderReady() {
//        mediaRecorder = new MediaRecorder();
//        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
//        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
//        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
////        mediaRecorder.setOutputFile(FrontEngine.getInstance().AudioSavePathInDevice);
//    }

}
