package com.droid.notesapp.helper;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.StrictMode;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.droid.notesapp.CustomDialog;
import com.droid.notesapp.SpinnerDialog;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;

public class GlobalHelperNormal {

    public CustomDialog dialog = null;

    private GlobalHelperNormal() {

    }

    private static GlobalHelperNormal objGlobalHelperNormal = null;

    public static GlobalHelperNormal getInstance() {
        if (objGlobalHelperNormal == null) {
            objGlobalHelperNormal = new GlobalHelperNormal();
        }
        return objGlobalHelperNormal;
    }

//    public void shareCodeIntent(final Activity activity, final Context c, final String subject, final String body) {
//        final AlertDialog aDialog = showSpotProgressDialog(c, "Please wait...");
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                // Do something after 5s = 5000ms
//                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
//                sharingIntent.setType("text/plain");
//                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
//                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, body);
//                activity.startActivity(Intent.createChooser(sharingIntent, c.getResources().getString(R.string.shareApp)));
//                aDialog.dismiss();
//            }
//        }, 500);
//    }

//    public void shareCodeIntent(final Fragment f, final Context c, final String subject, final String body) {
//        final AlertDialog aDialog = showSpotProgressDialog(c, "Please wait...");
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                // Do something after 5s = 5000ms
//                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
//                sharingIntent.setType("text/plain");
//                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
//                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, body);
//                f.startActivity(Intent.createChooser(sharingIntent, c.getResources().getString(R.string.shareApp)));
//                aDialog.dismiss();
//            }
//        }, 500);
//    }

    public void ignoreFileUriExposure() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }

    public String convertTimeZone(String dateStr, String format, TimeZone timeZoneActual, TimeZone timeZoneConvert) {
        SimpleDateFormat df = new SimpleDateFormat(format);
        df.setTimeZone(timeZoneActual);
        Date date = null;
        try {
            date = df.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        df.setTimeZone(timeZoneConvert);
        return df.format(date);
    }

    public String getPreviousOrNextDate(int days, String format, String dateString) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        Calendar cal = Calendar.getInstance();
        Date date = null;
        try {
            date = sdf.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        cal.setTime(date);
        cal.add(Calendar.DATE, days);
        return sdf.format(cal.getTime());
    }

    //compressing image with given width height and set result in PictureReszie inrerface
//    public void compressImage(PictureResize pictureResize, File file, float maxWidth, float maxHeight) {
//        FileOutputStream out = null;
//
//        String filePath = file.getPath();
//        Bitmap scaledBitmap = null;
//
//        BitmapFactory.Options options = new BitmapFactory.Options();
//
////      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
////      you try the use the bitmap here, you will get null.
//        options.inJustDecodeBounds = true;
//        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);
//
//        int actualHeight = options.outHeight;
//        int actualWidth = options.outWidth;
//
////      max Height and width values of the compressed image is taken as 1280x720
//
//        float imgRatio = actualWidth / actualHeight;
//        float maxRatio = maxWidth / maxHeight;
//
////      width and height values are set maintaining the aspect ratio of the image
//        if (actualHeight > maxHeight || actualWidth > maxWidth) {
//            if (imgRatio < maxRatio) {
//                imgRatio = maxHeight / actualHeight;
//                actualWidth = (int) (imgRatio * actualWidth);
//                actualHeight = (int) maxHeight;
//            } else if (imgRatio > maxRatio) {
//                imgRatio = maxWidth / actualWidth;
//                actualHeight = (int) (imgRatio * actualHeight);
//                actualWidth = (int) maxWidth;
//            } else {
//                actualHeight = (int) maxHeight;
//                actualWidth = (int) maxWidth;
//
//            }
//        }
//
////      setting inSampleSize value allows to load a scaled down version of the original image
//
//        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
//
////      inJustDecodeBounds set to false to load the actual bitmap
//        options.inJustDecodeBounds = false;
//
////      this options allow android to claim the bitmap memory if it runs low on memory
//        options.inPurgeable = true;
//        options.inInputShareable = true;
//        options.inTempStorage = new byte[16 * 1024];
//
//        try {
////          load the bitmap from its path
//            bmp = BitmapFactory.decodeFile(filePath, options);
//        } catch (OutOfMemoryError exception) {
//            exception.printStackTrace();
//
//        }
//        try {
//            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
//        } catch (OutOfMemoryError exception) {
//            exception.printStackTrace();
//        }
//
//        float ratioX = actualWidth / (float) options.outWidth;
//        float ratioY = actualHeight / (float) options.outHeight;
//        float middleX = actualWidth / 2.0f;
//        float middleY = actualHeight / 2.0f;
//
//        Matrix scaleMatrix = new Matrix();
//        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);
//
//        Canvas canvas = new Canvas(scaledBitmap);
//        canvas.setMatrix(scaleMatrix);
//        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));
//
////      check the rotation of the image and display it properly
//        ExifInterface exif;
//        try {
//            exif = new ExifInterface(filePath);
//
//            int orientation = exif.getAttributeInt(
//                    ExifInterface.TAG_ORIENTATION, 0);
//            Log.d("EXIF", "Exif: " + orientation);
//            Matrix matrix = new Matrix();
//            if (orientation == 6) {
//                matrix.postRotate(90); //90
//                Log.d("EXIF", "Exif: " + orientation);
//            } else if (orientation == 3) {
//                matrix.postRotate(180);
//                Log.d("EXIF", "Exif: " + orientation);
//            } else if (orientation == 8) {
//                matrix.postRotate(270); //270
//                Log.d("EXIF", "Exif: " + orientation);
//            }
//            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
//                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
//                    true);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        //String filename = mediaFile.getPath();
//
//        try {
//
//            out = new FileOutputStream(filePath);
//            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
//
//            int w = scaledBitmap.getWidth();
//            int h = scaledBitmap.getHeight();
//
//            pictureResize.resizePicture(new File(filePath));
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//            pictureResize.resizePicture(null);
//        }
//
//
//    }

    //compressing image with given width height and set result in PictureReszie inrerface
//    public void compressImage(Activity activity, File file, float maxWidth, float maxHeight) {
//        FileOutputStream out = null;
//
//        String filePath = file.getPath();
//        Bitmap scaledBitmap = null;
//
//        BitmapFactory.Options options = new BitmapFactory.Options();
//
////      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
////      you try the use the bitmap here, you will get null.
//        options.inJustDecodeBounds = true;
//        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);
//
//        int actualHeight = options.outHeight;
//        int actualWidth = options.outWidth;
//
////      max Height and width values of the compressed image is taken as 1280x720
//
//        float imgRatio = actualWidth / actualHeight;
//        float maxRatio = maxWidth / maxHeight;
//
////      width and height values are set maintaining the aspect ratio of the image
//        if (actualHeight > maxHeight || actualWidth > maxWidth) {
//            if (imgRatio < maxRatio) {
//                imgRatio = maxHeight / actualHeight;
//                actualWidth = (int) (imgRatio * actualWidth);
//                actualHeight = (int) maxHeight;
//            } else if (imgRatio > maxRatio) {
//                imgRatio = maxWidth / actualWidth;
//                actualHeight = (int) (imgRatio * actualHeight);
//                actualWidth = (int) maxWidth;
//            } else {
//                actualHeight = (int) maxHeight;
//                actualWidth = (int) maxWidth;
//
//            }
//        }
//
////      setting inSampleSize value allows to load a scaled down version of the original image
//
//        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
//
////      inJustDecodeBounds set to false to load the actual bitmap
//        options.inJustDecodeBounds = false;
//
////      this options allow android to claim the bitmap memory if it runs low on memory
//        options.inPurgeable = true;
//        options.inInputShareable = true;
//        options.inTempStorage = new byte[16 * 1024];
//
//        try {
////          load the bitmap from its path
//            bmp = BitmapFactory.decodeFile(filePath, options);
//        } catch (OutOfMemoryError exception) {
//            exception.printStackTrace();
//
//        }
//        try {
//            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
//        } catch (OutOfMemoryError exception) {
//            exception.printStackTrace();
//        }
//
//        float ratioX = actualWidth / (float) options.outWidth;
//        float ratioY = actualHeight / (float) options.outHeight;
//        float middleX = actualWidth / 2.0f;
//        float middleY = actualHeight / 2.0f;
//
//        Matrix scaleMatrix = new Matrix();
//        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);
//
//        Canvas canvas = new Canvas(scaledBitmap);
//        canvas.setMatrix(scaleMatrix);
//        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));
//
////      check the rotation of the image and display it properly
//        ExifInterface exif;
//        try {
//            exif = new ExifInterface(filePath);
//
//            int orientation = exif.getAttributeInt(
//                    ExifInterface.TAG_ORIENTATION, 0);
//            Log.d("EXIF", "Exif: " + orientation);
//            Matrix matrix = new Matrix();
//            if (orientation == 6) {
//                matrix.postRotate(90); //90
//                Log.d("EXIF", "Exif: " + orientation);
//            } else if (orientation == 3) {
//                matrix.postRotate(180);
//                Log.d("EXIF", "Exif: " + orientation);
//            } else if (orientation == 8) {
//                matrix.postRotate(270); //270
//                Log.d("EXIF", "Exif: " + orientation);
//            }
//            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
//                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
//                    true);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        //String filename = mediaFile.getPath();
//        PictureResize pictureResize = (PictureResize) activity;
//
//        try {
//
//            out = new FileOutputStream(filePath);
//            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
//
//            int w = scaledBitmap.getWidth();
//            int h = scaledBitmap.getHeight();
//
//            pictureResize.resizePicture(new File(filePath));
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//            pictureResize.resizePicture(null);
//        }
//
//
//    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

//    public void sendNotification(Context context, String title, String messageBody,
//                                 EventData myEventsListData, SharedPreference objSP) {
//        Intent intent = new Intent(context, HomeActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        intent.putExtra(context.getString(R.string.obj_event), myEventsListData);
//
//        PendingIntent pendingIntent = PendingIntent.getActivity(context, objSP.getIntValue(objSP.notification_req_code),
//                intent,PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);
//        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
//                .setSmallIcon(R.mipmap.ic_launcher)
//                .setContentTitle(title)
//                .setContentText(messageBody)
//                .setAutoCancel(true)
//                .setSound(defaultSoundUri)
//                .setContentIntent(pendingIntent);
//        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//        notificationManager.notify(objSP.getIntValue(objSP.notification_req_code), notificationBuilder.build());
//        objSP.saveValueInSharedPreference(objSP.notification_req_code, objSP.getIntValue(objSP.notification_req_code) + 1);
//    }

    /**
     * Used to set multiple Alarms
     *
     * @param context
     * @param time
     */
//    public void setAlarm(Context context, long time, EventData myEventsListData, SharedPreference objSP, Class<?> cls) {
//        // create an Intent and set the class which will execute when Alarm triggers, here we have
//        // given AlarmReciever in the Intent, the onRecieve() method of this class will execute when
//        // alarm triggers and
//        //we will write the code to send SMS inside onRecieve() method pf Alarmreciever class
//        Intent intentAlarm = new Intent(context, cls);
//        intentAlarm.putExtra(context.getString(R.string.obj_event), myEventsListData);
//
//        // create the object
//        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
//
//        //set the alarm for particular time
//        alarmManager.set(AlarmManager.RTC_WAKEUP, time,
//                PendingIntent.getBroadcast(context, objSP.getIntValue(objSP.event_req_code), intentAlarm, PendingIntent.FLAG_UPDATE_CURRENT));
//        Toast.makeText(context, objSP.getIntValue(objSP.event_req_code) + " Alarm set for " + myEventsListData.getEvent_name()
//                , Toast.LENGTH_LONG).show();
//        objSP.saveValueInSharedPreference(objSP.event_req_code, objSP.getIntValue(objSP.event_req_code) + 1);
//    }

    /**
     * Used to set and repeat multiple Alarms
     */
//    public void setRepeatAlarm(Context context, long time, long repeatTime, EventData myEventsListData, SharedPreference objSP, Class<?> cls) {
//        // create an Intent and set the class which will execute when Alarm triggers, here we have
//        // given AlarmReciever in the Intent, the onRecieve() method of this class will execute when
//        // alarm triggers and
//        //we will write the code to send SMS inside onRecieve() method pf Alarmreciever class
//        Intent intentAlarm = new Intent(context, cls);
//        intentAlarm.putExtra(context.getString(R.string.obj_event), myEventsListData);
//
//        // create the object
//        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
//
//        //set the alarm for particular time
//        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, time, repeatTime,
//                PendingIntent.getBroadcast(context, objSP.getIntValue(objSP.event_req_code), intentAlarm, PendingIntent.FLAG_UPDATE_CURRENT));
//        Toast.makeText(context, objSP.getIntValue(objSP.event_req_code) + " Alarm set for " + myEventsListData.getEvent_name()
//                , Toast.LENGTH_LONG).show();
//        objSP.saveValueInSharedPreference(objSP.event_req_code, objSP.getIntValue(objSP.event_req_code) + 1);
//    }

    //call create event dialog function start
    public CustomDialog callDialog(Activity activity, String title, String desc) {
        dialog = new CustomDialog(activity, title, desc, new CustomDialog.OnClickSelection() {
            @Override
            public void setOk() {
                if (dialog != null)
                    dialog.dismiss();

            }
        });

        if (!dialog.isShowing()) {
            dialog.show();
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(true);
        }
        return dialog;
    }

    /**
     * Used to show custom dialog
     * <p>
     * //     * @param layoutId
     * //     * @param c
     * //     * @param cancelable
     * //     * @param theme
     * //     * @return
     */
    public Dialog showDialog(int layoutId, Context c, boolean cancelable, int theme) {
        Dialog dialog = new Dialog(c, theme);
        Window window = dialog.getWindow();
//        window.getAttributes().windowAnimations = R.style.DialogAnimationFade;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            //window.setStatusBarColor(ContextCompat.getColor(c, R.color.bg_home_header));
        }
        window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(layoutId);
        //Show the dialog!
        dialog.show();
        dialog.setCancelable(cancelable);

        return dialog;
    }

    /**
     * Used to show custom dialog
     *
     * @param layoutId
     * @param c
     * @param cancelable
     * @param theme
     * @return
     */
    public Dialog showDialog(int layoutId, final Context c, boolean cancelable, int theme, int animation) {
        Dialog dialog = new Dialog(c, theme);
        Window window = dialog.getWindow();
        window.getAttributes().windowAnimations = animation;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            //window.setStatusBarColor(ContextCompat.getColor(c, R.color.bg_home_header));
        }
        window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(layoutId);
        //Show the dialog!
        dialog.show();
        dialog.setCancelable(cancelable);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
//                windowImmersive((Activity)c);
            }
        });

        return dialog;
    }

    public AlertDialog.Builder showAlertDialog(Context context, String title, String msg, int icon, boolean cancelable,
                                               int posBtnText, DialogInterface.OnClickListener posBtnListner,
                                               int negBtnText, DialogInterface.OnClickListener negBtnListner,
                                               int nuetBtnText, DialogInterface.OnClickListener nuetBtnListner) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(msg);
        //alertDialogBuilder.setIcon(icon);
        alertDialogBuilder.setCancelable(cancelable);
        alertDialogBuilder.setPositiveButton(posBtnText, posBtnListner);
        alertDialogBuilder.setNegativeButton(negBtnText, negBtnListner);
        alertDialogBuilder.setNeutralButton(nuetBtnText, nuetBtnListner);
        alertDialogBuilder.show();

        return alertDialogBuilder;
    }

    /**
     * Used to show alert dialog
     *
     * @param context
     * @param title
     * @param msg
     * @param icon
     * @param cancelable
     * @param posBtnText
     * @param posBtnListner
     * @param negBtnText
     * @param negBtnListner
     * @param nuetBtnText
     * @param nuetBtnListner
     * @return
     */
    public AlertDialog.Builder showAlertDialog(Context context, int title, int msg, int icon, boolean cancelable,
                                               int posBtnText, DialogInterface.OnClickListener posBtnListner,
                                               int negBtnText, DialogInterface.OnClickListener negBtnListner,
                                               int nuetBtnText, DialogInterface.OnClickListener nuetBtnListner) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(msg);
        //alertDialogBuilder.setIcon(icon);
        alertDialogBuilder.setCancelable(cancelable);
        alertDialogBuilder.setPositiveButton(posBtnText, posBtnListner);
        alertDialogBuilder.setNegativeButton(negBtnText, negBtnListner);
        alertDialogBuilder.setNeutralButton(nuetBtnText, nuetBtnListner);
        alertDialogBuilder.show();

        return alertDialogBuilder;
    }

    /**
     * Used to show progress dialog
     *
     * @param context
     * @param msg
     * @param style
     * @return
     */
    public ProgressDialog showProgressDialog(Context context, String title, String msg, int style, int max, int progress, boolean indeterminate) {
        ProgressDialog pDialog = new ProgressDialog(context);
        pDialog.setTitle(title);
        pDialog.setMessage(msg);
        pDialog.setIndeterminate(indeterminate);
        pDialog.setCancelable(false);
        pDialog.setMax(max);
        pDialog.setProgress(progress);
        pDialog.setProgressStyle(style);

        return pDialog;
    }

    /**
     * Used to show spot progress dialog
     *
     * @param context
     * @param msg
     * @return
     */
//    public AlertDialog showSpotProgressDialog(Context context, String msg) {
//        AlertDialog aDialog = new SpotsDialog(context, msg, R.style.Custom_Spot_Dialog);
//        aDialog.setCancelable(false);
//        aDialog.show();
//        return aDialog;
//    }

    /**
     * Show Date Picker Dialog
     *
     * @param context
     * @param onDateSetListener
     */
    public void showDatePickerDialog(Context context, DatePickerDialog.OnDateSetListener onDateSetListener) {
        Calendar c = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                onDateSetListener, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    /**
     * Show Time Picker Dialog
     *
     * @param context
     * @param onTimeSetListener
     */
    public void showTimePickerDialog(Context context, TimePickerDialog.OnTimeSetListener onTimeSetListener) {
        Calendar c = Calendar.getInstance();
        TimePickerDialog timePickerDialog = new TimePickerDialog(context,
                onTimeSetListener, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), false);
        timePickerDialog.show();
    }

    /**
     * Function to convert milliseconds time to
     * Timer Format
     * Hours:Minutes:Seconds
     */
    public String milliSecondsToTimer(long milliseconds) {
        String finalTimerString = "";
        String secondsString = "";

        // Convert total duration into time
        int hours = (int) (milliseconds / (1000 * 60 * 60));
        int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);
        // Add hours if there
        if (hours > 0) {
            finalTimerString = hours + ":";
        }

        // Prepending 0 to seconds if it is one digit
        if (seconds < 10) {
            secondsString = "0" + seconds;
        } else {
            secondsString = "" + seconds;
        }

        finalTimerString = finalTimerString + minutes + ":" + secondsString;

        // return timer string
        return finalTimerString;
    }

    /**
     * Used to convert String type date into milliseconds
     *
     * @param dateTime
     * @param format
     * @return
     */
    public long convertStringDateTimeIntoMiliSec(String dateTime, String format) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            Date date = sdf.parse(dateTime);
            Log.d("time", "convertStringDateTimeIntoMiliSec: " + date.getTime());
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * Used to get specific time or date according to given format
     *
     * @param format
     * @param year
     * @param month
     * @param day
     * @param hour
     * @param min
     * @param sec
     * @return
     */
    public String getSpecificTimeOrDate(String format, int year, int month, int day, int hour, int min, int sec) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        Calendar newDate = Calendar.getInstance();
        newDate.set(year, month, day, hour, min, sec);
        return sdf.format(newDate.getTime());
    }

    /**
     * Used to get current time or date according to given format
     *
     * @param format
     * @return
     */
    public String getCurrentTimeOrDate(String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(Calendar.getInstance().getTime());
    }

    public String changeTimeOrDateFormat(String oldFormat, String newFormat, String time) {
        SimpleDateFormat sdfOldFormat = new SimpleDateFormat(oldFormat);
        SimpleDateFormat sdfNewFormat = new SimpleDateFormat(newFormat);
        try {
            return sdfNewFormat.format(sdfOldFormat.parse(time));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * Used to get normal rounded image by using glide
     *
     * @param c
     * @param iv
     * @param url
     * @return
     */
    public String setGlideRoundedImage(Context c, ImageView iv, String url, ProgressBar pb, int placeHolder, float cornerRadius) {
        if (url != null) {
            Glide.with(c)
                    .load(url)
                    .asBitmap()
                    .centerCrop()
                    .placeholder(placeHolder)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .listener(reqListener(iv, pb))
                    .into(bitmabIVTargetRounded(iv, c, cornerRadius));
        }
        return url;
    }

    /**
     * Make image view as rounded bitmap image view
     *
     * @param iv
     * @param c
     * @return
     */
    private BitmapImageViewTarget bitmabIVTargetRounded(final ImageView iv, final Context c, final float cornerRadius) {
        BitmapImageViewTarget mBitmapImageViewTarget = new BitmapImageViewTarget(iv) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(c.getResources(), resource);
                circularBitmapDrawable.setCornerRadius(cornerRadius);
                (iv).setImageDrawable(circularBitmapDrawable);
            }
        };
        return mBitmapImageViewTarget;
    }

    /**
     * Used to get normal image by using glide
     *
     * @param c
     * @param iv
     * @param url
     * @return
     */
    public String setGlideNormalImage(Context c, ImageView iv, String url, ProgressBar pb, int placeHolder) {
        if (url != null) {
            Glide.with(c)
                    .load(url)
                    .asBitmap()
                    .centerCrop()
                    .placeholder(placeHolder)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .listener(reqListener(iv, pb))
                    .into(iv);
        }
        return url;
    }

    public String setGlideNormalImageFit(Context c, ImageView iv, String url, ProgressBar pb, int placeHolder) {
        if (url != null) {
            Glide.with(c)
                    .load(url)
                    .asBitmap()
                    .fitCenter()
                    .placeholder(placeHolder)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .listener(reqListener(iv, pb))
                    .into(iv);
        }
        return url;
    }

    /**
     * Used to get circular image by using glide
     *
     * @param c
     * @param iv
     * @param url
     * @return
     */
    public String setGlideCircularImage(Context c, ImageView iv, String url, ProgressBar pb, int placeHolder) {
//        pb.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(c, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
        if (url != null) {
            Glide.with(c)
                    .load(url)
                    .asBitmap()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .placeholder(placeHolder)
                    .listener(reqListener(iv, pb))
                    .into(bitmabIVTarget(iv, c));
        }
        return url;
    }

    public String setGlideCircularImageFit(Context c, ImageView iv, String url, ProgressBar pb, int placeHolder) {
//        pb.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(c, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
        if (url != null) {
            Glide.with(c)
                    .load(url)
                    .asBitmap()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .fitCenter()
                    .placeholder(placeHolder)
                    .listener(reqListener(iv, pb))
                    .into(bitmabIVTarget(iv, c));
        }
        return url;
    }

    /**
     * Make image view as circular bitmap image view
     *
     * @param iv
     * @param c
     * @return
     */
    private BitmapImageViewTarget bitmabIVTarget(final ImageView iv, final Context c) {
        BitmapImageViewTarget mBitmapImageViewTarget = new BitmapImageViewTarget(iv) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(c.getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                (iv).setImageDrawable(circularBitmapDrawable);
            }
        };
        return mBitmapImageViewTarget;
    }

    /**
     * Glide request listener
     * <p>
     * //     * @param iv
     *
     * @return
     */
    private RequestListener reqListener(final ImageView iv, final ProgressBar pb) {
        RequestListener mRequestListener = new RequestListener<String, Bitmap>() {
            @Override
            public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                Log.d("Glide", "onException: ");
                try {
                    pb.setVisibility(View.INVISIBLE);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
                return false;
            }

            @Override
            public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                Log.d("Glide", "onResourceReady: ");
                try {
                    pb.setVisibility(View.INVISIBLE);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
                return false;
            }
        };
        return mRequestListener;
    }

//    /**
//     * Used to check specific validation on text change
//     *
//     * @param et
//     * @param regex
//     * @param msg
//     * @param activity
//     */
//    public void checkSpecificValidationOnTextChange(final EditText et, final String regex, final String msg, final Activity activity) {
//        et.addTextChangedListener(new TextWatcher() {
//            // after every change has been made to this editText, we would like
//            // to check validity
//            public void afterTextChanged(Editable s) {
//                try {
//                    activity.runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            new Validation().isEditText(et, regex, msg, true);
//                        }
//                    });
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//            }
//
//            public void beforeTextChanged(CharSequence s, int start, int count,
//                                          int after) {
//            }
//
//            public void onTextChanged(CharSequence s, int start, int before,
//                                      int count) {
//            }
//        });
//    }
//
//    /**
//     * Used to check empty validation on text change
//     *
//     * @param et
//     * @param activity
//     */
//    public void checkValidationOnTextChange(final EditText et, final Activity activity) {
//        et.addTextChangedListener(new TextWatcher() {
//            public void afterTextChanged(Editable s) {
//                try {
//                    activity.runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            new Validation().hasText(et);
//                        }
//                    });
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//
//            public void beforeTextChanged(CharSequence s, int start, int count,
//                                          int after) {
//            }
//
//            public void onTextChanged(CharSequence s, int start, int before,
//                                      int count) {
//            }
//        });
//    }

//    public void shareCodeIntent(final Fragment f, final Context c, final String subject, final String body) {
//        final AlertDialog aDialog = showSpotProgressDialog(c, "Please wait...");
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                // Do something after 5s = 5000ms
//                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
//                sharingIntent.setType("text/plain");
//                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
//                sharingIntent.putExtra(Intent.EXTRA_TEXT, body);
//                f.startActivity(Intent.createChooser(sharingIntent, c.getResources().getString(R.string.share_code)));
//                aDialog.dismiss();
//            }
//        }, 500);
//    }

    public void showSoftKeyboard(Context c, EditText et) {
        try {
            InputMethodManager imm = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(et, InputMethodManager.SHOW_IMPLICIT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideSoftKeyboard(Context context, EditText et) {
        try {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) context.getSystemService(
                            context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    et.getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public void hideSoftKeyboardOnClick(EditText et, final Activity activity) {
//        et.addTextChangedListener(new TextWatcher() {
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//            }
//
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count,
//                                          int after) {
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                if (s.length() > 0) {
//                    if (s.charAt(s.length() - 1) == '\n') {
//                        hideSoftKeyboard(activity);
//                    }
//                }
//            }
//        });
//    }

//    public void sendNotification(Context context, String title, String messageBody, SharedPreference objSP) {
//        Intent intent = new Intent(context, HomeActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        //intent.putExtra(context.getString(R.string.obj_reminder), objReminderModel);
//
//        PendingIntent pendingIntent = PendingIntent.getActivity(context, objSP.getIntValue(objSP.reminder_notification_req_code),
//                intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
//                .setSmallIcon(R.mipmap.ic_launcher)
//                .setContentTitle(title)
//                .setContentText(messageBody)
//                .setAutoCancel(true)
//                .setContentIntent(pendingIntent);
//        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//        notificationManager.notify(objSP.getIntValue(objSP.reminder_notification_req_code), notificationBuilder.build());
//        objSP.saveValueInSharedPreference(objSP.reminder_notification_req_code, objSP.getIntValue(objSP.reminder_notification_req_code) + 1);
//    }

    public void cancelAlarm(Context context, Class<?> cls, int requestCode) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        // Cancel alarms
        try {
            alarmManager.cancel(PendingIntent.getBroadcast
                    (context, requestCode, new Intent(context, cls), PendingIntent.FLAG_CANCEL_CURRENT));
        } catch (Exception e) {

        }
    }

    /**
     * Used to set multiple Alarms
     * <p>
     * //     * @param context
     * //     * @param time
     */

    public byte[] serialize(Object obj) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ObjectOutputStream os = new ObjectOutputStream(out);
        os.writeObject(obj);
        return out.toByteArray();
    }

    public Object deserialize(byte[] data) throws IOException, ClassNotFoundException {
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        ObjectInputStream is = new ObjectInputStream(in);
        return is.readObject();
    }

    //Spinner dialog start from here
    public void spinnerStart(Activity act, SpinnerDialog spinnerDialog) {
        if (spinnerDialog != null && !spinnerDialog.isShowing()) {
            spinnerDialog.setCancelable(false);
            spinnerDialog.setCanceledOnTouchOutside(false);
            spinnerDialog.show();
        }
    }

    //    //Spinner dialog end
//
    public void spinnerStop(SpinnerDialog spinnerDialog) {
        if (spinnerDialog != null)
            spinnerDialog.dismiss();
    }

    /**
     * Used to logOut
     *
     * @param c
     * @param clsGo
     */
    public void logOut(Context c, Class<?> clsGo, SharedPreference prefs) {
        prefs.removeValue(prefs.check_login);
        Intent intent = new Intent(c, clsGo);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        c.startActivity(intent);
    }

    public int randInt(int min, int max) {
        ArrayList<Byte> arrListRandomIndexes = new ArrayList<Byte>();
        if (max > 10) {
            Random rand = new Random();
            return rand.nextInt((max - min) + 1) + min;
        } else if (max <= 10) {

            for (int i = min; i <= max; i++) {
                arrListRandomIndexes.add((byte) i);
            }
            Collections.shuffle(arrListRandomIndexes);
            return arrListRandomIndexes.get(0);
        }

        return 0;
    }
}
