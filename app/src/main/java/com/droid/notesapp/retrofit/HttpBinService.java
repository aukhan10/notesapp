package com.droid.notesapp.retrofit;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.HeaderMap;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Url;

/**
 * Created by talib on 1/20/2017.
 */
public interface HttpBinService {


    @POST("{url}")
    Call<JsonElement> postData(@Path(value = "url", encoded = true) String url, @HeaderMap HashMap<String, String> header, @Body JsonObject jsonObject);

    @PUT
    Call<JsonElement> putData(@Url String url, @HeaderMap HashMap<String, String> header, @Body JsonObject jsonObject);

    @GET
    Call<JsonElement> getData(@Url String url, @HeaderMap HashMap<String, String> header);

    @GET
    Call<JsonElement> getData(@Url String url);

    //@DELETE
    @HTTP(method = "DELETE", /*path = "post/delete",*/ hasBody = true)
    Call<JsonElement> deleteData(@Url String url, @HeaderMap HashMap<String, String> header, @Body JsonObject jsonObject);

    //@DELETE
    @HTTP(method = "DELETE", /*path = "post/delete",*/ hasBody = false)
    Call<JsonElement> deleteData(@Url String url, @HeaderMap HashMap<String, String> header);

    @POST("{url}")
    @Multipart
    Call<JsonElement> postProfilePicture(@Path("url") String url, @HeaderMap HashMap<String, String> header,
                                         @Part MultipartBody.Part image, @Part("share_with") RequestBody[] name);


    @POST("user/profile/upload")
    @Multipart
    Call<JsonElement> upLoadPicture(
            @HeaderMap HashMap<String, String> header,
            @Part MultipartBody.Part body,
            @Part("id") RequestBody userID
    );

    @POST("add-statement.json")
    @Multipart
    Call<JsonElement> uploadStatement(
            @HeaderMap HashMap<String, String> header,
            @Part MultipartBody.Part body,
            @Part("category_id") RequestBody category_id,
            @Part("title") RequestBody title,
            @Part("description") RequestBody description
    );

    @POST("update-statement.json")
    @Multipart
    Call<JsonElement> updateStatement(
            @HeaderMap HashMap<String, String> header,
            @Part MultipartBody.Part body,
            @Part("id") RequestBody id,
            @Part("category_id") RequestBody category_id,
            @Part("title") RequestBody title,
            @Part("description") RequestBody description
    );

    @POST("property/upload")
    @Multipart
    Call<JsonElement> upLoadPropertyImages(
            @HeaderMap HashMap<String, String> header,
            @Part MultipartBody.Part[] images,
            @Part("id") RequestBody propertyID,
            @Part("type") RequestBody propertyType
    );

    @POST("property/upload")
    @Multipart
    Call<JsonElement> upLoadMLSPropertyImages(
            @HeaderMap HashMap<String, String> header,
            @Part("file[0]") RequestBody images,
            @Part("id") RequestBody propertyID,
            @Part("type") RequestBody propertyType
    );
}
