package com.droid.notesapp.retrofit;

import com.droid.notesapp.helper.FrontEngine;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by talib on 1/20/2017.
 */

public class RetrofitFactory {
    public static final int POST = 1, GET = 2, PUT = 3, DELETE = 4;
    Retrofit retrofit;
    Call<JsonElement> j = null;
    HttpBinService service;

    public void requestServiceT(int methodType, HashMap map, String postURL, JsonObject jsonElement, CallBackRetrofit callback) {
        HttpBinService service = getServiceInstanceT(postURL + "/");

        if (methodType == POST)
            j = service.postData(postURL, map, jsonElement);
        else if (methodType == PUT)
            j = service.putData(postURL, map, jsonElement);
        else if (methodType == GET)
            j = service.getData(postURL, map);
        else if (methodType == DELETE) {
            if (jsonElement != null)
                j = service.deleteData(postURL, map, jsonElement);
            else
                j = service.deleteData(postURL, map);
        }

        j.enqueue(callback);
    }

    public HttpBinService getServiceInstanceT(String postURL) {

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();
        OkHttpClient.Builder okHttpClient = new OkHttpClient().newBuilder();
        okHttpClient.connectTimeout(1, TimeUnit.MINUTES);
        okHttpClient.readTimeout(1, TimeUnit.MINUTES);
        okHttpClient.writeTimeout(1, TimeUnit.MINUTES);
        okHttpClient.followRedirects(false);
        okHttpClient.followSslRedirects(false);
        OkHttpClient okHttpClient_ = okHttpClient.build();
        retrofit = new Retrofit.Builder()
                .baseUrl("https://translation.googleapis.com/")
                .client(okHttpClient_)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        service = retrofit.create(HttpBinService.class);


        return service;
    }

    public void requestService(int methodType, HashMap map, String postURL, JsonObject jsonElement, CallBackRetrofit callback) {
        HttpBinService service = getServiceInstance(postURL + "/");

        if (methodType == POST)
            j = service.postData(postURL, map, jsonElement);
        else if (methodType == PUT)
            j = service.putData(postURL, map, jsonElement);
        else if (methodType == GET)
            j = service.getData(postURL, map);
        else if (methodType == DELETE) {
            if (jsonElement != null)
                j = service.deleteData(postURL, map, jsonElement);
            else
                j = service.deleteData(postURL, map);
        }

        j.enqueue(callback);
    }

    public HttpBinService getServiceInstance(String postURL) {

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();
        OkHttpClient.Builder okHttpClient = new OkHttpClient().newBuilder();
        okHttpClient.connectTimeout(1, TimeUnit.MINUTES);
        okHttpClient.readTimeout(1, TimeUnit.MINUTES);
        okHttpClient.writeTimeout(1, TimeUnit.MINUTES);
        okHttpClient.followRedirects(false);
        okHttpClient.followSslRedirects(false);
        OkHttpClient okHttpClient_ = okHttpClient.build();
        retrofit = new Retrofit.Builder()
                .baseUrl(FrontEngine.getInstance().MainApiUrl)
                .client(okHttpClient_)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        service = retrofit.create(HttpBinService.class);


        return service;
    }

    public void requestSparkService(int methodType, HashMap map, String postURL, JsonObject jsonElement, CallBackRetrofit callback) {
        HttpBinService service = getSparkServiceInstance(postURL + "/");

        if (methodType == POST)
            j = service.postData(postURL, map, jsonElement);
        else if (methodType == PUT)
            j = service.putData(postURL, map, jsonElement);
        else if (methodType == GET)
            j = service.getData(postURL, map);
        else if (methodType == DELETE) {
            if (jsonElement != null)
                j = service.deleteData(postURL, map, jsonElement);
            else
                j = service.deleteData(postURL, map);
        }

        j.enqueue(callback);
    }


    public HttpBinService getSparkServiceInstance(String postURL) {

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();
        OkHttpClient.Builder okHttpClient = new OkHttpClient().newBuilder();
        okHttpClient.connectTimeout(1, TimeUnit.MINUTES);
        okHttpClient.readTimeout(1, TimeUnit.MINUTES);
        okHttpClient.writeTimeout(1, TimeUnit.MINUTES);
        okHttpClient.followRedirects(false);
        okHttpClient.followSslRedirects(false);
        OkHttpClient okHttpClient_ = okHttpClient.build();
        retrofit = new Retrofit.Builder()
                .baseUrl("https://api.worldweatheronline.com/premium/v1/")
                .client(okHttpClient_)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        service = retrofit.create(HttpBinService.class);
        return service;
    }

    public void upLoadPicture(
            HashMap<String, String> header,
            MultipartBody.Part body,
            RequestBody userID,
            CallBackRetrofit callBack) {

        HttpBinService service = getServiceInstance("");
        Call<JsonElement> j = service.upLoadPicture(header, body,
                userID
        );

        j.enqueue(callBack);

    }

    public void uploadStatement(
            HashMap<String, String> header,
            MultipartBody.Part body,
            RequestBody category_id,
            RequestBody title,
            RequestBody description,
            CallBackRetrofit callBack) {

        HttpBinService service = getServiceInstance("");
        Call<JsonElement> j = service.uploadStatement(header, body,
                category_id, title, description
        );

        j.enqueue(callBack);

    }

    public void updateStatement(
            HashMap<String, String> header,
            MultipartBody.Part body,
            RequestBody id,
            RequestBody category_id,
            RequestBody title,
            RequestBody description,
            CallBackRetrofit callBack) {

        HttpBinService service = getServiceInstance("");
        Call<JsonElement> j = service.updateStatement(header, body,
                id, category_id, title, description
        );

        j.enqueue(callBack);

    }

    public void upLoadPropertyImages(
            HashMap<String, String> header,
            MultipartBody.Part[] images,
            RequestBody propertyID,
            RequestBody propertyType,
            CallBackRetrofit callBack) {

        HttpBinService service = getServiceInstance("");
        Call<JsonElement> j = service.upLoadPropertyImages(header, images,
                propertyID, propertyType
        );

        j.enqueue(callBack);

    }

    public void upLoadMLSPropertyImages(
            HashMap<String, String> header,
            RequestBody images,
            RequestBody propertyID,
            RequestBody propertyType,
            CallBackRetrofit callBack) {

        HttpBinService service = getServiceInstance("");
        Call<JsonElement> j = service.upLoadMLSPropertyImages(header, images,
                propertyID, propertyType
        );

        j.enqueue(callBack);

    }

    public void cancelRequest() {
        j.cancel();
    }

    public boolean isExceutedCall() {
        if (j.isExecuted()) {
            return true;
        } else {
            return false;
        }
    }
}
