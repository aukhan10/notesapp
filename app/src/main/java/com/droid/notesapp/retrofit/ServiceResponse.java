package com.droid.notesapp.retrofit;


import org.json.JSONException;

/**
 * Created by talib on 3/14/2016.
 */


public interface ServiceResponse {

    void onResult(int type, HttpResponse o);
    void parseDataInBackground(int type, HttpResponse o);
    void onError(int type, HttpResponse o, Exception e) throws JSONException;
    void noInternetAccess(int type);
    void requestHttpCall(int type, String... params);


}
